package com.tanchengjin.admin.controller;

import com.github.pagehelper.PageHelper;
import com.tanchengjin.admin.model.pojo.ChinaArea;
import com.tanchengjin.admin.service.ChinaAreaService;
import com.tanchengjin.util.LayuiAdminServerResponse;
import com.tanchengjin.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 中国省市区控制器
 *
 * @author TanChengjin
 * @version v1.0.0
 * @since v1.0.0
 */
@Controller
@RequestMapping("/${app.admin.prefix}/chinaArea")
public class ChinaAreaController {
    /**
     * Service
     */
    @Autowired
    private ChinaAreaService chinaAreaService;

    @GetMapping("/index")
    public String index(Model view) {
        List<ChinaArea> areaList = chinaAreaService.getAll();
        view.addAttribute("areaList", areaList);
        return "/admin/chinaArea/list";
    }

    @RequestMapping(value = "/list", method = {RequestMethod.GET})
    @ResponseBody
    public LayuiAdminServerResponse index(@RequestParam(value = "page", defaultValue = "1") int page, @RequestParam(value = "limit", defaultValue = "10") int limit, ChinaArea chinaArea) {
        PageHelper.startPage(page, limit);
        List<ChinaArea> chinaAreaList = chinaAreaService.getAllByCondition(chinaArea);
        long count = chinaAreaService.countByCondition(chinaArea);
        return LayuiAdminServerResponse.responseWithSuccess(chinaAreaList, String.valueOf(count));
    }

    @RequestMapping(value = "/create", method = {RequestMethod.GET})
    public String create(Model view) {
        List<ChinaArea> areaList = chinaAreaService.getAll();
        view.addAttribute("areaList", areaList);
        return "/admin/chinaArea/add";
    }

    /**
     * <p>
     * POST http://www.example.com HTTP/1.1
     * Content-Type: application/json;charset=UTF-8
     * </p>
     *
     * @return ServerResponse
     */
    @RequestMapping(value = "", method = {RequestMethod.POST})
    @ResponseBody
    public ServerResponse store(@RequestBody ChinaArea chinaArea) {
        int i = chinaAreaService.create(chinaArea);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("创建失败请重试");
        }
    }

    /**
     * <p>
     * POST http://www.example.com HTTP/1.1
     * Content-Type: application/x-www-form-urlencoded;charset=UTF-8
     * </p>
     *
     * @return ServerResponse
     */
    @RequestMapping(value = "/submit", method = {RequestMethod.POST})
    @ResponseBody
    public ServerResponse submit(ChinaArea chinaArea) {
        int i = chinaAreaService.create(chinaArea);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("创建失败请重试");
        }
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    @ResponseBody
    public ServerResponse<ChinaArea> show(@PathVariable("id") int id) {
        ChinaArea chinaArea = chinaAreaService.findOneById(id);
        return ServerResponse.responseWithSuccess(chinaArea);
    }

    @RequestMapping(value = "/{id}/edit", method = {RequestMethod.GET})
    public String edit(Model view, @PathVariable(value = "id") int id) {
        ChinaArea chinaArea = chinaAreaService.findOneById(id);
        view.addAttribute("chinaArea", chinaArea);
        List<ChinaArea> areaList = chinaAreaService.getAll();
        view.addAttribute("areaList", areaList);
        return "/admin/chinaArea/edit";
    }

    /**
     * <p>
     * POST http://www.example.com HTTP/1.1
     * Content-Type: application/json;charset=UTF-8
     * </p>
     *
     * @return ServerResponse
     */
    @RequestMapping(value = "/{id}", method = {RequestMethod.PUT})
    @ResponseBody
    public ServerResponse update(@RequestBody ChinaArea chinaArea, @PathVariable("id") int id) {
        int i = chinaAreaService.updateById(chinaArea, id);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("更新失败，请重试");
        }
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.DELETE})
    @ResponseBody
    public ServerResponse destroy(@PathVariable(value = "id") int id) {
        int i = chinaAreaService.deleteById(id);

        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("删除失败请重试");
        }
    }

    @RequestMapping(value = "/batchDelete", method = {RequestMethod.DELETE})
    @ResponseBody
    public ServerResponse batchDelete(@RequestParam(value = "ids[]") int[] ids) {
        int i = chinaAreaService.batchDelete(ids);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("删除失败请重试");
        }
    }

    /**
     * <p>
     * POST http://www.example.com HTTP/1.1
     * Content-Type: application/x-www-form-urlencoded;charset=UTF-8
     * </p>
     *
     * @return ServerResponse
     */
    @RequestMapping(value = "/{id}/change", method = {RequestMethod.PUT})
    @ResponseBody
    public ServerResponse change(ChinaArea chinaArea, @PathVariable("id") int id) {
        int i = chinaAreaService.updateById(chinaArea, id);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("更新失败，请重试");
        }
    }
}