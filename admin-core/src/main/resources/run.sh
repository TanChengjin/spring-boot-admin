#!/bin/bash
#运行环境
env=@spring.application.profiles@
#获取项目包名
jarName=@build.finalName@.jar
pid=`ps -ef|grep ${jarName}|grep -v grep|awk '{print $2}'`
if [ -n "$pid" ]
then
  echo "正在结束${jarName}已存在的进程"
  kill -9 $pid
fi
echo "启动${jarName}服务"
nohup java -jar -Dspring.profiles.active=${env} ${jarName} >/dev/null 2>&1 &