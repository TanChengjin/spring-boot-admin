package com.tanchengjin.admin.model.pojo;

import java.util.Date;

public class AdminRolePermission {
    private Integer id;

    private Integer adminRoleId;

    private Integer adminPermissionId;

    private Date createdAt;

    private Date updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAdminRoleId() {
        return adminRoleId;
    }

    public void setAdminRoleId(Integer adminRoleId) {
        this.adminRoleId = adminRoleId;
    }

    public Integer getAdminPermissionId() {
        return adminPermissionId;
    }

    public void setAdminPermissionId(Integer adminPermissionId) {
        this.adminPermissionId = adminPermissionId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}