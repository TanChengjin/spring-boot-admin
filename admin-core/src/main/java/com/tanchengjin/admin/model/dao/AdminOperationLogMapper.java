package com.tanchengjin.admin.model.dao;

import com.tanchengjin.admin.model.pojo.AdminOperationLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * admin_operation_logMapper
 *
 * @author  TanChengjin
 * @since   v1.0.0
 * @version v1.0.0
 */
@Mapper
public interface AdminOperationLogMapper {

    int deleteByPrimaryKey(int id);

    int insert(AdminOperationLog record);

    int insertSelective(AdminOperationLog record);

    AdminOperationLog selectByPrimaryKey(int id);

    int updateByPrimaryKeySelective(AdminOperationLog record);

    int updateByPrimaryKey(AdminOperationLog record);

    List<AdminOperationLog> getAll();

    List<AdminOperationLog> getAllByCondition(AdminOperationLog condition);

    long count();

    int batchDeleteByPrimaryKey(int[] ids);

    int logicalBatchDeleteByPrimaryKey(int[] ids);

}
