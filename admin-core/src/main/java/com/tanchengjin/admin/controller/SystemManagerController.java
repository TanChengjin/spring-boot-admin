package com.tanchengjin.admin.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootVersion;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import oshi.SystemInfo;
import oshi.hardware.GlobalMemory;

import java.util.*;

/**
 * 系统类，包括框架信息、系统信息等
 *
 * @author TanChengjin
 */
@RequestMapping("${app.admin.prefix}/system")
@Controller
public class SystemManagerController {
    static List<String> oshi = new ArrayList<>();

    @Value("${spring.profiles.active:}")
    private String profile;

    @Value("${java.version:version:}")
    private String javaVersion;

    @Value("${application:name:}")
    private String name;

    @Value("${build.version:}")
    private String buildVersion;

    @Value("${build.timestamp:}")
    private String buildTimestamp;
    @Value("${app.admin.version}")
    private String version;

    /**
     * 系统信息
     */
    @RequestMapping("/info")
    @ResponseBody
    public Map info() {
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("profile", profile);
        stringObjectHashMap.put("javaVersion", javaVersion);
        stringObjectHashMap.put("name", name);
        stringObjectHashMap.put("buildVersion", buildVersion);
        stringObjectHashMap.put("buildTimestamp", buildTimestamp);
        stringObjectHashMap.put("version", version);
        stringObjectHashMap.put("springBootVersion", SpringBootVersion.getVersion());

        return stringObjectHashMap;
    }

    /**
     * 系统Memory信息
     */
    @RequestMapping("/memoryInfo")
    @ResponseBody
    public Map memoryInfo() {
        SystemInfo systemInfo = new SystemInfo();
        GlobalMemory memory = systemInfo.getHardware().getMemory();
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("available", memory.getAvailable());
        stringObjectHashMap.put("total", memory.getTotal());
        stringObjectHashMap.put("swapTotal", memory.getSwapTotal());
        stringObjectHashMap.put("swapUsed", memory.getSwapUsed());
        stringObjectHashMap.put("swapPagesIn", memory.getSwapPagesIn());
        stringObjectHashMap.put("swapPageSize", memory.getPageSize());
        stringObjectHashMap.put("swapPageOut", memory.getSwapPagesOut());
        return stringObjectHashMap;
    }
}
