package com.tanchengjin.admin.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@Component
@ConfigurationProperties(prefix = "app.admin")
@Primary
public class AdminConfig implements Serializable {
    private static final long serialVersionUID = -8188735035883619684L;

    private String prefix = "/admin";

    private String name = "admin manager";

    /**
     * 调试模式
     */
    @Deprecated
    private Boolean debug = false;
//    /**
//     * jwt签名
//     */
//    @Deprecated
//    private String jwtSign;
    /**
     * 当前版本
     */
    private String version = "v1.0.0";
    /**
     * 存储路径(保存一些上传的文件)
     */
    private String storagePath = "/storage";

    public Boolean isDebug() {
        if (this.debug == null) debug = false;
        return debug;
    }
}
