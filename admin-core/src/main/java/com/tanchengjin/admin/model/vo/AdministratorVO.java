package com.tanchengjin.admin.model.vo;

import com.tanchengjin.admin.model.pojo.AdminMenu;
import com.tanchengjin.admin.model.pojo.AdminUser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AdministratorVO implements Serializable {
    private static final long serialVersionUID = 8394925825362189828L;

    private AdminUser userinfo = new AdminUser();
    //当前用户可用的所有菜单
    private List<AdminMenu> menus = new ArrayList<AdminMenu>();

    public AdminUser getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(AdminUser userinfo) {
        this.userinfo = userinfo;
    }

    public List<AdminMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<AdminMenu> menus) {
        this.menus = menus;
    }
}
