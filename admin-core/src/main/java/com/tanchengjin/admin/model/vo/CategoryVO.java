package com.tanchengjin.admin.model.vo;

import java.util.Date;

@Deprecated
public class CategoryVO {
    private Integer id;

    private Integer rank;

    private String name;

    private Integer parentId;

    private Integer sort;

    private Integer level;

    private Boolean isDirectory;

    private Boolean enable;

    private String path;

    private Date createdAt;

    private Date updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}