CREATE DATABASE IF NOT EXISTS `admindb`;

USE `admindb`;


DROP TABLE IF EXISTS `admin_config`;

CREATE TABLE `admin_config`
(
    `id`         int(10) unsigned NOT NULL AUTO_INCREMENT,
    `key`        varchar(100)     NOT NULL COMMENT '配置项',
    `content`    text             NOT NULL COMMENT '配置值json格式',
    `created_at` datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `key` (`key`),
    KEY `key_2` (`key`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8 COMMENT ='配置表';


insert into `admin_config`(`id`, `key`, `content`, `created_at`, `updated_at`)
values (1, 'website', '{\"websiteName\":\"index\"}', '2022-06-17 20:43:09', '2022-06-17 20:43:09');


DROP TABLE IF EXISTS `admin_config_email`;

CREATE TABLE `admin_config_email`
(
    `id`         int(10) unsigned NOT NULL AUTO_INCREMENT,
    `host`       varchar(100)     NOT NULL,
    `port`       varchar(30)      NOT NULL,
    `username`   varchar(100)     NOT NULL,
    `password`   varchar(100)     NOT NULL,
    `created_at` datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8 COMMENT ='邮箱服务配置表';


insert into `admin_config_email`(`id`, `host`, `port`, `username`, `password`, `created_at`, `updated_at`)
values (1, '', '25', '', '', '2022-03-16 11:38:18', '2022-03-16 11:38:18');


DROP TABLE IF EXISTS `admin_menu`;

CREATE TABLE `admin_menu`
(
    `id`           int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name`         varchar(30)      NOT NULL COMMENT '菜单名称',
    `url`          varchar(255)     NOT NULL COMMENT 'url路径',
    `is_directory` tinyint(1)                DEFAULT '0' COMMENT '是否为目录',
    `description`  varchar(100)              DEFAULT NULL COMMENT '描述',
    `icon`         varchar(30)               DEFAULT NULL COMMENT '图标',
    `parent_id`    int(10) unsigned          DEFAULT NULL COMMENT '上级菜单',
    `sort`         int(8)           NOT NULL DEFAULT '100' COMMENT '排序值',
    `created_at`   datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`   datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `fk_admin_menu_parent_id` (`parent_id`),
    CONSTRAINT `fk_admin_menu_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `admin_menu` (`id`) ON DELETE SET NULL
) ENGINE = InnoDB
  AUTO_INCREMENT = 69
  DEFAULT CHARSET = utf8 COMMENT ='菜单表';


insert into `admin_menu`(`id`, `name`, `url`, `is_directory`, `description`, `icon`, `parent_id`, `sort`, `created_at`,
                         `updated_at`)
values (40, '主页', '#', 1, '主页菜单', 'layui-icon-home', NULL, 10, '2021-11-18 16:31:05', '2022-03-16 15:40:08'),
       (41, '控制台', '/welcome', 1, '控制台菜单', 'layui-icon-home', 40, 101, '2021-11-18 16:32:44', '2022-03-16 14:26:24'),
       (42, '文章管理', '/#', 1, '文章管理', 'layui-icon-app', NULL, 20, '2021-11-19 20:46:31', '2022-03-16 15:40:04'),
       (43, '文章列表', '/articleManager/index', 1, '文章列表菜单', 'layui-icon-app', 42, 300, '2021-11-19 20:55:54',
        '2021-12-04 20:16:09'),
       (44, '分类管理', '/categoryManager/index', 1, '文章分类管理', 'layui-icon-app', 42, 200, '2021-11-19 20:56:29',
        '2021-12-04 20:15:55'),
       (45, '评论管理', '/articleCommentManager/index', 1, '文章评论管理菜单', 'layui-icon-app', 42, 100, '2021-11-19 20:57:11',
        '2021-12-04 20:16:03'),
       (46, '管理员中心', '/#', 1, '用户管理菜单', 'layui-icon-user', NULL, 50, '2021-11-19 20:58:45', '2022-03-16 15:40:39'),
       (47, '网站用户', '/userManager/index', 1, '网站用户管理菜单', 'layui-icon-user', 46, 600, '2021-11-19 21:00:03',
        '2021-11-20 14:14:08'),
       (48, '后台管理员', '/administrators/user/index', 1, '后台管理员用户管理', 'layui-icon-user', 46, 200, '2021-11-19 21:01:17',
        '2021-11-19 21:01:17'),
       (49, '角色管理', '/administrators/role/index', 1, '角色管理', 'layui-icon-user', 46, 300, '2021-11-19 21:02:51',
        '2021-11-20 17:40:00'),
       (50, '权限管理', '/administrators/permission/index', 1, '权限管理', 'layui-icon-user', 46, 400, '2021-11-19 21:03:46',
        '2021-11-20 17:40:00'),
       (51, '菜单管理', '/administrators/menu/index', 1, '菜单管理', 'layui-icon-user', 46, 500, '2021-11-19 21:04:44',
        '2021-11-20 17:39:05'),
       (58, '系统设置', '/#', 1, '系统设置', 'layui-icon-set', NULL, 100, '2021-11-26 09:51:09', '2022-03-16 15:41:23'),
       (59, '网站设置', '/settings/website', 0, '网站设置', '#', 58, 300, '2021-11-26 09:53:12', '2022-03-16 14:10:14'),
       (60, '邮件服务', '/settings/email', 0, '邮件服务', '#', 58, 200, '2021-11-26 10:04:36', '2021-11-26 10:04:36'),
       (61, '个人中心', '/#', 1, '管理员的个人中心', 'layui-icon-username', NULL, 30, '2021-11-26 11:27:30', '2022-03-16 15:39:58'),
       (62, '基本信息', '/administrator/baseInfo', 1, '管理员基本信息', 'layui-icon-username', 61, 100, '2021-11-26 11:29:08',
        '2021-11-26 11:29:08'),
       (63, '修改密码', '/administrator/password', 1, '管理员用户修改密码', 'layui-icon-username', 61, 200, '2021-11-26 11:29:54',
        '2021-11-26 11:29:54'),
       (64, '字典管理', '#', 1, '', 'layui-icon-tabs', NULL, 40, '2021-12-06 16:04:05', '2022-03-16 15:40:51'),
       (65, '省市区管理', '/chinaArea/index', 1, '中国行政区管理', 'layui-icon-auz', 64, 100, '2021-12-06 16:05:06',
        '2021-12-06 16:05:06'),
       (67, '系统管理', '#', 1, '系统管理', 'layui-icon-date', NULL, 10100, '2021-12-06 21:14:09', '2021-12-06 21:14:09'),
       (68, '操作日志', '/adminOperationLog/index', 0, '', 'layui-icon-log', 67, 100, '2021-12-06 21:14:47',
        '2022-03-16 14:10:04');


DROP TABLE IF EXISTS `admin_operation_log`;

CREATE TABLE `admin_operation_log`
(
    `id`          bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `description` varchar(255)        NOT NULL DEFAULT '',
    `ip`          varchar(30)         NOT NULL DEFAULT '',
    `username`    varchar(100)        NOT NULL DEFAULT '',
    `url`         varchar(200)        NOT NULL COMMENT '当前访问url',
    `http_method` varchar(20)         NOT NULL COMMENT '当前请求method',
    `headers`     text COMMENT '记录header头 json格式',
    `params`      text COMMENT '记录param所有参数 json格式',
    `created_at`  datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`  datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at`  datetime                     DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='管理员操作日志';



DROP TABLE IF EXISTS `admin_permission`;

CREATE TABLE `admin_permission`
(
    `id`           int(10) unsigned NOT NULL AUTO_INCREMENT,
    `key`          varchar(100)     NOT NULL COMMENT '唯一标识',
    `name`         varchar(100)     NOT NULL COMMENT '权限名称',
    `http_method`  varchar(30)      NOT NULL COMMENT 'HTTP请求方法',
    `http_path`    varchar(100)     NOT NULL COMMENT '权限路径',
    `parent_id`    int(10) unsigned          DEFAULT NULL COMMENT '上级权限',
    `target`       varchar(100)     NOT NULL DEFAULT '_self' COMMENT '页面打开方式',
    `description`  varchar(150)              DEFAULT NULL COMMENT '描述',
    `icon`         varchar(130)     NOT NULL DEFAULT '',
    `identitifier` varchar(255)     NOT NULL DEFAULT '' COMMENT '权限判断标识符',
    `type`         tinyint(2)       NOT NULL DEFAULT '1' COMMENT '权限类型',
    `sort`         int(10)          NOT NULL DEFAULT '0' COMMENT '排序',
    `enable`       tinyint(1)       NOT NULL DEFAULT '1' COMMENT '是否可用',
    `created_at`   datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`   datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 27
  DEFAULT CHARSET = utf8 COMMENT ='权限表';


insert into `admin_permission`(`id`, `key`, `name`, `http_method`, `http_path`, `parent_id`, `target`, `description`,
                               `icon`, `identitifier`, `type`, `sort`, `enable`, `created_at`, `updated_at`)
values (1, '', '主页', 'ALL', '#', NULL, '_self', '主页', 'layui-icon-home', '', 1, 0, 1, '2022-06-10 15:04:12',
        '2022-06-17 20:20:09'),
       (2, 'dashboardd', '控制台', 'ALL', '/welcome', 1, '_self', '控制台', 'layui-icon-home', '', 2, 0, 1,
        '2022-06-10 14:25:45', '2022-06-10 16:56:13'),
       (3, '', '系统管理', 'ALL', '#', NULL, '_self', '', 'layui-icon-date', '', 1, 0, 1, '2022-06-10 16:42:11',
        '2022-06-17 20:20:08'),
       (4, '', '操作日志', 'ALL', '/adminOperationLog/index', 3, '_self', '', 'layui-icon-log', '', 2, 0, 1,
        '2022-06-10 16:43:04', '2022-06-10 16:49:51'),
       (8, '', '个人中心', 'ALL', '#', NULL, '_self', NULL, 'layui-icon-username', '', 1, 0, 1, '2022-06-10 16:46:46',
        '2022-06-10 16:46:57'),
       (9, '', '基本信息', 'ALL', '/administrator/baseInfo', 8, '_self', '基本信息', 'layui-icon-username', '', 2, 0, 1,
        '2022-06-10 16:49:06', '2022-06-10 16:55:30'),
       (10, '', '修改密码', 'ALL', '/administrator/password', 8, '_self', '', 'layui-icon-username', '', 2, 0, 1,
        '2022-06-10 16:49:28', '2022-06-10 16:55:46'),
       (14, '', '系统设置', 'ALL', '#', NULL, '_self', '', 'layui-icon-set-sm', '', 1, 0, 1, '2022-06-10 16:57:31',
        '2022-06-10 16:57:31'),
       (15, '', '网站设置', 'ALL', '/settings/website', 14, '_self', '', 'layui-icon-rate', '', 2, 0, 1,
        '2022-06-10 16:58:15', '2022-06-10 16:59:50'),
       (16, '', '邮件设置', 'ALL', '/settings/email', 14, '_self', '', '', '', 2, 0, 1, '2022-06-10 17:00:29',
        '2022-06-17 20:57:30'),
       (17, '', '管理员中心', 'ALL', '#', NULL, '_self', '', 'layui-icon-username', '', 1, 0, 1, '2022-06-10 17:20:28',
        '2022-06-10 17:20:28'),
       (18, '', '网站用户', 'ALL', '/userManager/index', 17, '_self', '', '', '', 2, 0, 1, '2022-06-10 17:20:52',
        '2022-06-10 17:22:36'),
       (19, '', '后台管理员', 'ALL', '/administrators/user/index', 17, '_self', '', '', '', 2, 0, 1, '2022-06-10 17:21:11',
        '2022-06-10 17:22:39'),
       (20, '', '角色管理', 'ALL', '/administrators/role/index', 17, '_self', '', '', '', 2, 0, 1, '2022-06-10 17:21:26',
        '2022-06-10 17:22:39'),
       (22, '', '菜单管理', 'ALL', '/administrators/permission/index', 17, '_self', '', '', '', 2, 0, 1,
        '2022-06-10 17:22:17', '2022-06-10 17:22:40'),
       (23, '', '文章管理', 'ALL', '#', NULL, '_self', '', 'layui-icon-app', '', 1, 0, 1, '2022-06-10 17:25:44',
        '2022-06-10 17:25:44'),
       (24, '', '文章列表', 'ALL', '/articleManager/index', 23, '_self', '', '', '', 2, 0, 1, '2022-06-10 17:26:18',
        '2022-06-10 17:27:29'),
       (25, '', '分类列表', 'ALL', '/categoryManager/index', 23, '_self', '', '', '', 2, 0, 1, '2022-06-10 17:28:55',
        '2022-06-10 17:30:57'),
       (26, '', '评论列表', 'ALL', '/articleCommentManager/index', 23, '_self', '', '', '', 2, 0, 1, '2022-06-10 17:34:37',
        '2022-06-10 17:34:37');


DROP TABLE IF EXISTS `admin_permission_menu`;

CREATE TABLE `admin_permission_menu`
(
    `id`                  int(10) unsigned NOT NULL AUTO_INCREMENT,
    `admin_menu_id`       int(10) unsigned NOT NULL COMMENT '菜单ID',
    `admin_permission_id` int(10) unsigned NOT NULL COMMENT '权限ID',
    `created_at`          datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`          datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `fk_admin_permission_id` (`admin_permission_id`),
    KEY `fk_admin_menu` (`admin_menu_id`),
    CONSTRAINT `fk_admin_menu` FOREIGN KEY (`admin_menu_id`) REFERENCES `admin_menu` (`id`) ON DELETE CASCADE,
    CONSTRAINT `fk_admin_permission_id` FOREIGN KEY (`admin_permission_id`) REFERENCES `admin_permission` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='菜单权限表';


DROP TABLE IF EXISTS `admin_role`;

CREATE TABLE `admin_role`
(
    `id`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `role`        varchar(100)     NOT NULL COMMENT '角色',
    `name`        varchar(100)     NOT NULL COMMENT '角色名称',
    `description` varchar(100)     NOT NULL COMMENT '角色描述',
    `created_at`  datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`  datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `role` (`role`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8 COMMENT ='角色表';


insert into `admin_role`(`id`, `role`, `name`, `description`, `created_at`, `updated_at`)
values (1, 'administrator', '超级管理员', '至高无上的权力', '2022-03-16 11:38:17', '2022-03-16 11:38:17'),
       (2, 'article', '文章管理员', '负责文章的增删改查等操作', '2022-03-16 11:38:17', '2022-03-16 11:38:17'),
       (3, 'normal', '普通角色', '普通角色只能进入仪表盘没有任何权限', '2022-03-16 11:38:17', '2022-03-16 11:38:17');


DROP TABLE IF EXISTS `admin_role_permission`;

CREATE TABLE `admin_role_permission`
(
    `id`                  int(10) unsigned NOT NULL AUTO_INCREMENT,
    `admin_role_id`       int(10) unsigned NOT NULL COMMENT '角色ID',
    `admin_permission_id` int(10) unsigned NOT NULL COMMENT '权限ID',
    `created_at`          datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`          datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `fk_arp_admin_permission_id` (`admin_permission_id`),
    KEY `idx_admin_user_id` (`admin_role_id`),
    CONSTRAINT `fk_arp_admin_permission_id` FOREIGN KEY (`admin_permission_id`) REFERENCES `admin_permission` (`id`) ON DELETE CASCADE,
    CONSTRAINT `fk_arp_admin_role_id` FOREIGN KEY (`admin_role_id`) REFERENCES `admin_role` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 76
  DEFAULT CHARSET = utf8 COMMENT ='管理员权限表';


insert into `admin_role_permission`(`id`, `admin_role_id`, `admin_permission_id`, `created_at`, `updated_at`)
values (37, 1, 1, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (38, 1, 2, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (39, 1, 3, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (40, 1, 4, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (41, 1, 8, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (42, 1, 9, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (43, 1, 10, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (44, 1, 14, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (45, 1, 15, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (46, 1, 16, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (47, 1, 17, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (48, 1, 18, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (49, 1, 19, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (50, 1, 20, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (52, 1, 22, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (53, 1, 23, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (54, 1, 24, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (55, 1, 25, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (56, 1, 26, '2022-06-16 22:16:40', '2022-06-16 22:16:40'),
       (57, 3, 1, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (58, 3, 2, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (59, 3, 3, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (60, 3, 4, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (61, 3, 8, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (62, 3, 9, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (63, 3, 10, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (64, 3, 14, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (65, 3, 15, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (66, 3, 16, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (67, 3, 17, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (68, 3, 18, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (69, 3, 19, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (70, 3, 20, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (71, 3, 22, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (72, 3, 23, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (73, 3, 24, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (74, 3, 25, '2022-06-19 10:51:52', '2022-06-19 10:51:52'),
       (75, 3, 26, '2022-06-19 10:51:52', '2022-06-19 10:51:52');


DROP TABLE IF EXISTS `admin_user`;

CREATE TABLE `admin_user`
(
    `id`             int(10) unsigned    NOT NULL AUTO_INCREMENT,
    `username`       varchar(100)        NOT NULL COMMENT '用户名',
    `name`           varchar(100)        NOT NULL COMMENT '名称',
    `password`       varchar(100)        NOT NULL COMMENT '密码',
    `avatar_url`     varchar(200)                 DEFAULT NULL COMMENT '头像',
    `sex`            tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '性别,0未知,1男、2女',
    `salt`           varchar(100)                 DEFAULT NULL COMMENT '盐值',
    `email`          varchar(50)                  DEFAULT NULL COMMENT '邮箱',
    `phone`          varchar(20)                  DEFAULT NULL COMMENT '手机号',
    `enable`         tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否可用',
    `signature`      varchar(255)                 DEFAULT NULL COMMENT '个性签名',
    `remember_token` varchar(255)                 DEFAULT NULL COMMENT 'remember token',
    `created_at`     datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`     datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `username` (`username`, `email`, `phone`),
    KEY `idx_admin_user_name` (`username`),
    KEY `idx_admin_user_phone` (`phone`),
    KEY `idx_admin_user_email` (`email`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8 COMMENT ='管理员表';


insert into `admin_user`(`id`, `username`, `name`, `password`, `avatar_url`, `sex`, `salt`, `email`, `phone`, `enable`,
                         `signature`, `remember_token`, `created_at`, `updated_at`)
values (1, 'admin', 'admin', '$2a$10$ZTvphnvONZqcBOLuxEZM8.3Xo6704tf6Gkw67kinh.NNx8f1/ZKYG', NULL, 0,
        '$2a$10$O1JsJtlGv6zeGHxs05uXk.', 'admin@admin.com', '18888888888', 1, NULL, '769244ff51f3858b28922b418e24c935',
        '2022-03-16 11:38:17', '2022-06-19 11:13:23'),
       (2, 'article', 'article', '$2a$10$PjOuodhL5a6EHOnqAAd..u2Le7.DgrQdYYR3rbBNGkUhBeFe5NPru', NULL, 0,
        '$2a$10$O1JsJtlGv6zeGHxs05uXk.', 'article@article.com', '18888888881', 1, NULL,
        '114bc0a086b7f42d403472eb7b5bf92b', '2022-03-16 11:38:17', '2022-03-16 15:18:01'),
       (3, 'normal', 'normal', '$2a$10$8d43nHtC44Q030iWSzyNmOlIpAM/hJSIPk4TzGd3LlAWgqdZXY0QW', NULL, 0,
        '$2a$10$O1JsJtlGv6zeGHxs05uXk.', 'normal@normal.com', '18888888882', 1, NULL,
        '9729abd35df3d6a4c8dc868e5ae9e7e0', '2022-03-16 11:38:17', '2022-06-19 11:09:13');


DROP TABLE IF EXISTS `admin_user_role`;

CREATE TABLE `admin_user_role`
(
    `id`            int(10) unsigned NOT NULL AUTO_INCREMENT,
    `admin_user_id` int(10) unsigned NOT NULL COMMENT '管理员ID',
    `admin_role_id` int(10) unsigned NOT NULL COMMENT '角色ID',
    `created_at`    datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`    datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `fk_admin_role_id` (`admin_role_id`),
    KEY `idx_admin_user_id` (`admin_user_id`),
    CONSTRAINT `fk_admin_role_id` FOREIGN KEY (`admin_role_id`) REFERENCES `admin_role` (`id`) ON DELETE CASCADE,
    CONSTRAINT `fk_admin_user_id` FOREIGN KEY (`admin_user_id`) REFERENCES `admin_user` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8 COMMENT ='管理员角色表';


insert into `admin_user_role`(`id`, `admin_user_id`, `admin_role_id`, `created_at`, `updated_at`)
values (1, 1, 1, '2022-03-16 11:38:17', '2022-03-16 11:38:17'),
       (2, 2, 2, '2022-03-16 11:38:17', '2022-03-16 11:38:17'),
       (9, 3, 3, '2022-06-19 11:09:44', '2022-06-19 11:09:44');