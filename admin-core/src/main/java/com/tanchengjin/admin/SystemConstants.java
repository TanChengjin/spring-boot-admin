package com.tanchengjin.admin;

/**
 * 后台常量
 * @author tanchengjin
 */
public class SystemConstants {
    //管理员基本信息，与数据库对应
    public final static String adminUserSessionKey = "CURRENT_ADMIN_USER_SESSION_KEY";
    //管理员所有信息
    public final static String adminSessionKey = "CURRENT_ADMIN_SESSION_KEY";
}
