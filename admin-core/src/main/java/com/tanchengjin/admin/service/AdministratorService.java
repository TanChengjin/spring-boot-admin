package com.tanchengjin.admin.service;

import com.tanchengjin.admin.model.pojo.*;
import com.tanchengjin.admin.model.vo.AdministratorVO;

import java.security.Permission;
import java.util.List;

/**
 * 管理员服务层
 */
public interface AdministratorService {
    Integer getId();

    int createAdminUser(AdminUser adminUser, int[] roles);

    int createAdminUserRole(int[] roles, int adminId);

    int deleteRoleByAdminId(int adminId);

    int batchDelete(int[] ids);

    @Deprecated
    List<AdminUserRole> getRoleByAdminId(int id);

    @Deprecated
    List<AdminPermission> getPermissionByRoleId(int id);

    @Deprecated
    List<AdminPermission> getPermissionByAdminId(int id);

    int createAdminRole(AdminRole adminRole);

    int createAdminRolePermission(Integer[] permissionIds, int roleId);

    @Deprecated
    /**
     * @see replaced by {@link AdminPermissionService.getAll}
     */
    List<AdminPermission> getAdminPermissionList();

    @Deprecated
    public List<AdminMenu> getAdminMenuTree(int id);

    /**
     * 当前管理员信息，与数据库对应
     *
     * @return
     */
    @Deprecated
    AdminUser getCurrentUser();

    /**
     * 当前管理员菜单
     *
     * @return List<AdminMenu>
     */
    @Deprecated
    public List<AdminMenu> getCurrentAdminMenuList();

    @Deprecated
    public AdminUserExtension getAdminDetails();

    /**
     * 保存了当前管理员所有相关信息
     *
     * @return AdministratorVO
     */
    AdministratorVO getAdmin();

    /**
     * 修改密码
     *
     * @param password string
     * @param adminId  管理员ID
     * @return int
     */
    int changePassword(int adminId, String password);

    /**
     * 为角色分配权限
     *
     * @param roleId  角色id
     * @param menuIds 菜单ids
     * @return 添加的权限结果集
     */
    int allocPermission(int roleId, List<Integer> menuIds);


    List<AdminMenu> getMenu();

    AdminUser getUser();

    /**
     * 获取所有的按钮树
     */
    List<AdminPermission> getMenuTree();

    /**
     * 获取所有的标识权限
     */
    List<AdminPermission> getIdentifierPermission();

    /**
     * 获取管理员所拥有的全部角色
     */
    List<AdminRole> getRoles();

    /**
     * 获取管理员所拥有的全部角色id
     */
    List<Integer> getRoleIds();

    /**
     * 获取当前管理员所有权限
     */
    List<AdminPermission> getPermissions();
}
