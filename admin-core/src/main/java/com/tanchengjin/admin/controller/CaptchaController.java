package com.tanchengjin.admin.controller;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * @author tanchengjin
 */
@Controller
@RequestMapping("${app.admin.prefix}/captcha")
public class CaptchaController {
    private final static Logger logger = LoggerFactory.getLogger(CaptchaController.class);
    @Autowired
    private Producer captchaProducer;

    @RequestMapping("/image")
    public void getCaptchaImage(HttpServletResponse response, HttpServletRequest request) throws IOException {
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-store,no-cache,must-revalidate");
        response.setHeader("Cache-Control", "post-check=0,pre-check=0");
        response.setHeader("Pragma", "no-cache");
        //设置响应类型
        response.setContentType("image/png");

        String text = captchaProducer.createText();
        request.getSession().setAttribute(Constants.KAPTCHA_SESSION_KEY, text);
        Object attribute = request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        logger.info(attribute.toString());
        BufferedImage image = captchaProducer.createImage(text);
        ServletOutputStream outputStream = response.getOutputStream();
        ImageIO.write(image, "png", outputStream);
        try {
            outputStream.flush();
        } finally {
            outputStream.close();
        }
    }
}
