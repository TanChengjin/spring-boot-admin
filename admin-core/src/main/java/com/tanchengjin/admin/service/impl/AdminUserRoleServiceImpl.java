package com.tanchengjin.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tanchengjin.admin.model.dao.AdminUserRoleMapper;
import com.tanchengjin.admin.model.pojo.AdminUserRole;
import com.tanchengjin.admin.service.AdminUserRoleService;
import org.springframework.stereotype.Service;

/**
 * @author TanChengjin
 * @version v1.0.0
 * @email 18865477815@163.com
 */
@Service
public class AdminUserRoleServiceImpl extends ServiceImpl<AdminUserRoleMapper, AdminUserRole> implements AdminUserRoleService {
}
