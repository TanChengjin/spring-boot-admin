package com.tanchengjin.admin.model.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * create by  pojo
 *
 * @author TanChengjin
 * @since v1.0.0
 * @version v1.0.0
 */
//@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class AdminOperationLog implements Serializable {

    private static final long serialVersionUID = 1791082720232008134L;

    private Integer id;
    private String description;
    private String ip;
    /**
     * 当前访问url
     */
    private String url;
    /**
     * 当前访问url
     */
    private String httpMethod;
    /**
     * 记录header头 json格式
     */
    private String headers;
    /**
     * 记录param所有参数 json格式
     */
    private String params;
    /**
     * 操作者
     */
    private String operator;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date deletedAt;


    public Integer getId()
    {
        return this.id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getIp()
    {
        return this.ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getHttpMethod()
    {
        return this.httpMethod;
    }

    public void setHttpMethod(String httpMethod)
    {
        this.httpMethod = httpMethod;
    }

    public String getHeaders()
    {
        return this.headers;
    }

    public void setHeaders(String headers)
    {
        this.headers = headers;
    }

    public String getParams()
    {
        return this.params;
    }

    public void setParams(String params)
    {
        this.params = params;
    }

    public Date getCreatedAt()
    {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt)
    {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt()
    {
        return this.updatedAt;
    }

    public void setUpdatedAt(Date updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt()
    {
        return this.deletedAt;
    }

    public void setDeletedAt(Date deletedAt)
    {
        this.deletedAt = deletedAt;
    }


    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}