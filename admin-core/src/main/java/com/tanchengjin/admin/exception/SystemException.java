package com.tanchengjin.admin.exception;

public class SystemException extends RuntimeException{
    private static final long serialVersionUID = -8224136627896821995L;

    public SystemException(String message) {
        super(message);
    }
}
