package com.tanchengjin.admin.controller;

import com.tanchengjin.admin.model.pojo.AdminMenu;
import com.tanchengjin.admin.model.pojo.AdminPermission;
import com.tanchengjin.admin.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("${app.admin.prefix}")
public class AdminController {
    @Autowired
    private AdministratorService administratorService;

    @GetMapping("")
    public String index(Model model) {
        //获取当前用户的菜单目录
        List<AdminPermission> currentAdminMenuList = administratorService.getMenuTree();
        model.addAttribute("menuList", currentAdminMenuList);
        return "/admin/index/index";
    }

    @GetMapping("/dashboard")
    public String console() {
        return "/admin/index/console";
    }

    @GetMapping("welcome")
    public String welcome() {
        return "/admin/index/welcome";
    }

//    @GetMapping("/test")
//    @ResponseBody
//    public Object console1() {
//        return administratorService.getCurrentAdminMenuList();
//    }
}
