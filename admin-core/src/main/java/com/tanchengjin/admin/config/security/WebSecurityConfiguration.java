package com.tanchengjin.admin.config.security;

import com.tanchengjin.admin.config.AdminConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private AdminConfig adminConfig;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().permitAll();
        http.headers().frameOptions().disable();
        //排除某路径下的csrf
        http.csrf().ignoringAntMatchers(adminConfig.getPrefix()+"/upload/ueditor");
//        http.csrf().disable();
    }
}
