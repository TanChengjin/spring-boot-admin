package com.tanchengjin.admin.model.dao;

import com.tanchengjin.admin.model.pojo.AdminPermissionMenu;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AdminPermissionMenuMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AdminPermissionMenu record);

    int insertSelective(AdminPermissionMenu record);

    AdminPermissionMenu selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AdminPermissionMenu record);

    int updateByPrimaryKey(AdminPermissionMenu record);

    int deleteByPrimaryKeyIds(int[] ids);

    List<AdminPermissionMenu> selectByMenuId(int id);

    List<AdminPermissionMenu> selectByMenuIds(List<Integer> ids);

    List<AdminPermissionMenu> selectByPermissionId(int id);

    List<AdminPermissionMenu> selectByPermissionIds(List<Integer> ids);

    int deleteByMenuId(int id);
}