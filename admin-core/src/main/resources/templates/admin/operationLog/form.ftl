<input type="hidden" name="${(_csrf.parameterName)!''}" value="${(_csrf.token)!''}">

                <div class="layui-form-item">
                    <label class="layui-form-label required">DESCRIPTION</label>
                    <div class="layui-input-block">
                        <textarea type="text" name="description" lay-verify="required" autocomplete="off" class="layui-textarea">${(adminOperationLog.description)!''}</textarea>
                    </div>
                </div>
<div class="layui-form-item">
    <label class="layui-form-label required">IP</label>
    <div class="layui-input-block">
        <input type="text" name="ip" placeholder="请输入" autocomplete="off" lay-verify="required" class="layui-input" value="${(adminOperationLog.ip)!''}">
</div>
</div>
                <div class="layui-form-item">
                    <label class="layui-form-label required">当前访问url</label>
                    <div class="layui-input-block">
                        <textarea type="text" name="url" lay-verify="required" autocomplete="off" class="layui-textarea">${(adminOperationLog.url)!''}</textarea>
                    </div>
                </div>
<div class="layui-form-item">
    <label class="layui-form-label required">当前访问url</label>
    <div class="layui-input-block">
        <input type="text" name="httpMethod" placeholder="请输入" autocomplete="off" lay-verify="required" class="layui-input" value="${(adminOperationLog.httpMethod)!''}">
</div>
</div>
    <div class="layui-form-item">
    <label class="layui-form-label">记录header头 json格式</label>
    <div class="layui-input-block">
        <textarea type="text" name="headers" lay-verify="" autocomplete="off" class="layui-textarea">${(adminOperationLog.headers)!''}</textarea>
    </div>
</div>

    <div class="layui-form-item">
    <label class="layui-form-label">记录param所有参数 json格式</label>
    <div class="layui-input-block">
        <textarea type="text" name="params" lay-verify="" autocomplete="off" class="layui-textarea">${(adminOperationLog.params)!''}</textarea>
    </div>
</div>

        <div class="layui-form-item">
            <label class="layui-form-label required">CREATED_AT</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" id="CREATED_AT" placeholder="" name="createdAt" value="<#if (adminOperationLog.createdAt)??>${adminOperationLog.createdAt?string('yyyy-MM-dd HH:mm:ss')}</#if>">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label required">UPDATED_AT</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" id="UPDATED_AT" placeholder="" name="updatedAt" value="<#if (adminOperationLog.updatedAt)??>${adminOperationLog.updatedAt?string('yyyy-MM-dd HH:mm:ss')}</#if>">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">DELETED_AT</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" id="DELETED_AT" placeholder="" name="deletedAt" value="<#if (adminOperationLog.deletedAt)??>${adminOperationLog.deletedAt?string('yyyy-MM-dd HH:mm:ss')}</#if>">
            </div>
        </div>
