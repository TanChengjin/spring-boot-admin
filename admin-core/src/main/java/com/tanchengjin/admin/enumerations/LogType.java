package com.tanchengjin.admin.enumerations;

/**
 * 操作日志类型
 *
 * @author tanchengjin
 */
public enum LogType {
    NORMAL(1, "操作日志"),
    LOGIN(2, "认证日志");

    private final int code;
    private final String description;


    LogType(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
