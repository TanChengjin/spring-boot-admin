package com.tanchengjin.admin.model.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * create by  pojo
 *
 * @author TanChengjin
 * @version v1.0.0
 * @since v1.0.0
 */
//@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Data
public class AdminConfig implements Serializable {

    private static final long serialVersionUID = 1791082720232008134L;

    private Integer id;
    /**
     * 配置项
     */
    private String key;
    /**
     * 配置值json格式
     */
    private String content;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

}