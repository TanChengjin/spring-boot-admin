package com.tanchengjin.admin.config;

import com.tanchengjin.admin.interceptions.AdminAuthenticationInterceptor;
import com.tanchengjin.admin.interceptions.AdminPermissionInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.concurrent.TimeUnit;

@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {
    @Autowired
    private AdminConfig adminConfig;
    //以下路径不需要登录也可以访问
    private final String[] authenticationExcludePath = {"/auth/login", "/captcha/image"};

    //以下路径不进行权限检查
    private final String[] permissionExcludePath = {
            "/auth/login", "", "/", "/captcha/image", "/auth/logout"
    };

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        CacheControl cacheControl = CacheControl.maxAge(2, TimeUnit.HOURS);
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/").setCacheControl(cacheControl);
        //设置文件存储路径
        registry.addResourceHandler("/storage/**").addResourceLocations("file:///" + adminConfig.getStoragePath()).setCacheControl(cacheControl);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String adminPrefix = adminConfig.getPrefix();
        if (adminPrefix.endsWith("/") || adminPrefix.endsWith("\\")) {
            adminPrefix = adminPrefix.substring(0, adminPrefix.length() - 1);
        }
        //认证拦截器
        registry.addInterceptor(adminAuthenticationInterception()).addPathPatterns(adminPrefix + "/**").excludePathPatterns(getAuthenticationExcludePath());

        //权限检查拦截器
        registry.addInterceptor(adminPermissionInterceptor()).addPathPatterns(adminPrefix + "/**").excludePathPatterns(getPermissionExcludePath());
//        registry.addInterceptor(adminPermissionInterceptor()).addPathPatterns(adminPrefix + "/**").excludePathPatterns(adminPrefix + "/auth/login", adminPrefix + "/", adminPrefix);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AdminAuthenticationInterceptor adminAuthenticationInterception() {
        return new AdminAuthenticationInterceptor();
    }

    @Bean
    public AdminPermissionInterceptor adminPermissionInterceptor() {
        return new AdminPermissionInterceptor();
    }

    //    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**")//项目中的所有接口都支持跨域
//                .allowedOriginPatterns("*")//所有地址都可以访问，也可以配置具体地址
//                .allowCredentials(true)
//                .allowedMethods("*")//"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"
//                .maxAge(3600);// 跨域允许时间
//    }
    //添加前缀后的认证排除路径
    public String[] getAuthenticationExcludePath() {
        String prefix = adminConfig.getPrefix();
        int l = authenticationExcludePath.length;
        String[] r = new String[l];
        for (int i = 0; i < l; i++) {
            r[i] = prefix + authenticationExcludePath[i];
        }
        return r;
    }

    public String[] getPermissionExcludePath() {
        String prefix = adminConfig.getPrefix();
        int l = permissionExcludePath.length;
        String[] r = new String[l];
        for (int i = 0; i < l; i++) {
            r[i] = prefix + permissionExcludePath[i];
        }
        return r;
    }
}
