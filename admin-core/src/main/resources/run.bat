@echo on
:: #运行环境
set env=@spring.application.profiles@
:: #获取项目包名
SET jarName=@build.finalName@.jar
echo 启动%jarName%服务
java -jar -Dspring.profiles.active=%env% %jarName%
pause