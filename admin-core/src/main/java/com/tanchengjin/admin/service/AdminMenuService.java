package com.tanchengjin.admin.service;

import com.tanchengjin.admin.model.pojo.AdminMenu;
import com.tanchengjin.admin.service.impl.AdminMenuServiceImpl;

import java.util.List;

/**
 * 菜单表(AdminMenu) Service
 *
 * @author TanChengjin
 * @since 2021-11-15 17:06:56
 */
@Deprecated
public interface AdminMenuService {
    public AdminMenu findById(int id);

    public int deleteById(int id);

    public int insert(AdminMenu adminMenu);

    public int updateById(AdminMenu adminMenu, int id);

    public List<AdminMenu> getAll();

    public int batchDelete(int[] ids);

    public long count();

    int resetSort(int parent_id);

    int updateSort(int parent_id, AdminMenuServiceImpl.SortDirection direction, int targetId, Integer nodeId);

    int updateSort2(int parent_id, AdminMenuServiceImpl.SortDirection direction, int targetId, Integer nodeId);

    @Deprecated
    List<AdminMenu> getAllByAdminId(int id);

    /**
     * 获取所有菜单树,递归追加children
     *
     * @return List
     */
    List<AdminMenu> getNodeTree();

    /**
     * 简单菜单树 子节点不追加到children而是放到当前节点的后面
     *
     * @return List
     */
    List<AdminMenu> getSimpleNodeTree();

    List<AdminMenu> getMenuTreeByAdminId(int id);


    /**
     * 菜单树
     */
    List<AdminMenu> getTree();

    /**
     * 检查角色是否有菜单的权限
     *
     * @param roleId 角色id
     * @param menuId 菜单id
     * @return 是否拥有权限
     */
    @Deprecated
    public boolean checkRoleHasPermission(int roleId, int menuId);
}
