package com.tanchengjin.admin.service;

import com.tanchengjin.admin.model.pojo.AdminOperationLog;
import java.util.List;

/**
 * admin_operation_log Service
 *
 * @author TanChengjin
 * @since
 */
public interface AdminOperationLogService{

    AdminOperationLog findOneById(int id);

    int deleteById(int id);

    int create(AdminOperationLog record);

    int updateById(AdminOperationLog record,int id);

    List<AdminOperationLog>getAll();

    List<AdminOperationLog>getAllByCondition(AdminOperationLog condition);

    int batchDelete(int[] ids);

    long count();
}