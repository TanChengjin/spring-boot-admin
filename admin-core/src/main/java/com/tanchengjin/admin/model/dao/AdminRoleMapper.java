package com.tanchengjin.admin.model.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanchengjin.admin.model.pojo.AdminRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface AdminRoleMapper extends BaseMapper<AdminRole> {
    int deleteByPrimaryKey(int id);

    int insert(AdminRole record);

    int insertSelective(AdminRole record);

    AdminRole selectByPrimaryKey(int id);

    int updateByPrimaryKeySelective(AdminRole record);

    int updateByPrimaryKey(AdminRole record);

    List<AdminRole> getAll();

    int count();
}