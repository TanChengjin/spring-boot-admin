package com.tanchengjin.admin.model.dao;

import com.tanchengjin.admin.model.pojo.AdminConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * admin_configMapper
 *
 * @author TanChengjin
 * @version v1.0.0
 * @since v1.0.0
 */
@Mapper
public interface AdminConfigMapper {
    int deleteByPrimaryKey(int id);

    int batchDeleteByPrimaryKey(int id);

    int insert(AdminConfig record);

    int insertSelective(AdminConfig record);

    AdminConfig selectByPrimaryKey(int id);

    int updateByPrimaryKeySelective(AdminConfig record);

    int updateByPrimaryKey(AdminConfig record);

    List<AdminConfig> getAll();

    List<AdminConfig> getAllByCondition(AdminConfig condition);

    int count();

    int batchDeleteByPrimaryKey(int[] ids);

    AdminConfig selectByKey(String key);

}
