package com.tanchengjin.admin.model.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanchengjin.admin.model.pojo.AdminRolePermission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AdminRolePermissionMapper extends BaseMapper<AdminRolePermission> {
    int deleteByPrimaryKey(Integer id);

    int insert(AdminRolePermission record);

    int insertSelective(AdminRolePermission record);

    AdminRolePermission selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AdminRolePermission record);

    int updateByPrimaryKey(AdminRolePermission record);

    List<AdminRolePermission> selectByRoleId(int id);

    /**
     * 删除角色的所有权限
     *
     * @param id 角色id
     * @return 返回删除的结果集
     */
    int flushAllByRoleId(int id);

    /**
     * 批量插入
     *
     * @param roleId        角色id
     * @param permissionIds 权限ids
     * @return
     */
    int batchInsert(int roleId, List<Integer> permissionIds);
}