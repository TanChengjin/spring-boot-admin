<input type="hidden" name="${(_csrf.parameterName)!''}" value="${(_csrf.token)!''}">

<div class="layui-form-item">
    <label class="layui-form-label required">所属上级</label>

    <div class="layui-input-block">
        <select name="parentId" id="parentId" lay-search>
            <option value="">顶级</option>
            <#list areaList as area>
                <option value="${area.id}" <#if (chinaArea.parentId)?? && (chinaArea.parentId == area.id)>selected</#if>>${area.name}</option>
            </#list>
        </select>
    </div>
<#--    <div class="layui-input-block">-->
<#--        -->
<#--        <input type="text" name="parentId" placeholder="请输入" autocomplete="off" lay-verify="required number"-->
<#--               class="layui-input" value="<#if (chinaArea.parentId)??>${chinaArea.parentId?c}</#if>">-->
<#--    </div>-->
</div>
<div class="layui-form-item">
    <label class="layui-form-label required">地区代码</label>
    <div class="layui-input-block">
        <input type="text" name="code" placeholder="请输入" autocomplete="off" lay-verify="required" class="layui-input"
               value="${(chinaArea.code)!''}">
    </div>
</div>
<div class="layui-form-item">
    <label class="layui-form-label required">名称</label>
    <div class="layui-input-block">
        <input type="text" name="name" placeholder="请输入" autocomplete="off" lay-verify="required" class="layui-input"
               value="${(chinaArea.name)!''}">
    </div>
</div>