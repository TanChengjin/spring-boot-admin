package com.tanchengjin.admin.utils;

import java.util.Arrays;

public class IntUtil {
    /**
     * 判断数组是否包含某个键
     *
     * @Author TanChengjin
     * @Email 18865477815@163.com
     */
    public static boolean arrayContains(int[] arr, int key) {
        for (int ar : arr) {
            if (key == ar) {
                return true;
            }
        }
        return false;
    }


    /**
     * 排除数组中的某个键
     *
     * @Author TanChengjin
     * @Email 18865477815@163.com
     */
    public static int[] excludeByKey(int[] arr, int key) {
        //获取重复次数
        int rpt = countRepeat(arr, key);
        int[] na = new int[arr.length];

        if (rpt >= 1) {
            na = new int[arr.length - rpt];
        }

        int idx = 0;
        for (int i = 0; i < arr.length; i++) {
            if (key != arr[i]) {
                na[idx] = arr[i];
                ++idx;
            }
        }

        return na;
    }

    public static void main(String[] args) {
        int[] ids = {5, 3, 1, 1, 16, 5, 1, 1, 8, 5, 5, 1};
        int key = 1;

        System.out.println(Arrays.toString(excludeByKey(ids, key)));
    }

    /**
     * 统计数组某个键重复次数
     *
     * @Author TanChengjin
     * @Email 18865477815@163.com
     */
    public static int countRepeat(int[] array, int key) {
        int r = 0;
        for (int a : array) {
            if (a == key) {
                ++r;
            }
        }
        return r;
    }
}
