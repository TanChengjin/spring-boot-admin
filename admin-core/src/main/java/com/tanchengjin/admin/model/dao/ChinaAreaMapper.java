package com.tanchengjin.admin.model.dao;

import com.tanchengjin.admin.model.pojo.ChinaArea;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * china_areaMapper
 *
 * @author  TanChengjin
 * @since   v1.0.0
 * @version v1.0.0
 */
@Mapper
public interface ChinaAreaMapper {

    int deleteByPrimaryKey(int id);

    int insert(ChinaArea record);

    int insertSelective(ChinaArea record);

    ChinaArea selectByPrimaryKey(int id);

    int updateByPrimaryKeySelective(ChinaArea record);

    int updateByPrimaryKey(ChinaArea record);

    List<ChinaArea> getAll();

    List<ChinaArea> getAllByCondition(ChinaArea condition);

    int count();

    int batchDeleteByPrimaryKey(int[] ids);

    int countByCondition(ChinaArea condition);

}
