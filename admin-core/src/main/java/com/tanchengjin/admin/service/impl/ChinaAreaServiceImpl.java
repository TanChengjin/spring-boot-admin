package com.tanchengjin.admin.service.impl;

import com.tanchengjin.admin.model.dao.ChinaAreaMapper;
import com.tanchengjin.admin.model.pojo.ChinaArea;
import com.tanchengjin.admin.service.ChinaAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * create by  china_area ServiceImpl
 *
 * @author TanChengjin
 * @since v1.0.0
 * @version v1.0.0
 */
@Service("chinaAreaService")
public class ChinaAreaServiceImpl implements ChinaAreaService {

    @Autowired
    private ChinaAreaMapper chinaAreaMapper;

    public ChinaArea findOneById(int id)
    {
        ChinaArea chinaArea = chinaAreaMapper.selectByPrimaryKey(id);
        return chinaArea;
    }

    public int deleteById(int id)
    {
        int i = chinaAreaMapper.deleteByPrimaryKey(id);
        return i;

    }

    public int create(ChinaArea chinaArea)
    {
        int i = chinaAreaMapper.insertSelective(chinaArea);
        return i;
    }

    public int updateById(ChinaArea chinaArea, int id)
    {
        chinaArea.setId(id);
        int i = chinaAreaMapper.updateByPrimaryKeySelective(chinaArea);
        return i;
    }

    public List<ChinaArea> getAll()
    {
        List<ChinaArea> chinaAreaList = chinaAreaMapper.getAll();
        return chinaAreaList;
    }

    public List<ChinaArea> getAllByCondition(ChinaArea chinaArea)
    {
        List<ChinaArea> chinaAreaList = chinaAreaMapper.getAllByCondition(chinaArea);
        return chinaAreaList;
    }

    public int batchDelete(int[] ids)
    {
        int i = 0;
        for(int id : ids)
        {
            i+= chinaAreaMapper.deleteByPrimaryKey(id);
        }
        return i;
    }

    public int count()
    {
        return chinaAreaMapper.count();
    }

    @Override
    public int countByCondition(ChinaArea chinaArea) {
        return chinaAreaMapper.countByCondition(chinaArea);
    }
}