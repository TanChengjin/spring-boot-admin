package com.tanchengjin.admin.model.dao;

import com.tanchengjin.admin.model.pojo.AdminConfigEmail;
import org.apache.ibatis.annotations.Mapper;

/**
 * admin_config_emailMapper
 *
 * @author  TanChengjin
 * @since   v1.0.0
 * @version v1.0.0
 */
@Mapper
public interface AdminConfigEmailMapper {
    int updateByPrimaryKeySelective(AdminConfigEmail record);

    AdminConfigEmail selectByPrimaryKey(int i);
}
