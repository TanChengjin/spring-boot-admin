package com.tanchengjin.admin.controller;

import com.github.pagehelper.PageHelper;
import com.tanchengjin.admin.aop.needRole.NeedRole;
import com.tanchengjin.admin.model.pojo.AdminOperationLog;
import com.tanchengjin.admin.service.AdminOperationLogService;
import com.tanchengjin.util.LayuiAdminServerResponse;
import com.tanchengjin.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * admin_operation_log Controller
 *
 * @author  TanChengjin
 * @since   v1.0.0
 * @version v1.0.0
 */
@Controller
@RequestMapping("${app.admin.prefix}/adminOperationLog")
public class AdminOperationLogController {
    /**
    * service
    */
    @Autowired
    private AdminOperationLogService adminOperationLogService;

    @NeedRole("administrator")
    @GetMapping("/index")
    public String index() {
        return "/admin/operationLog/list";
    }
    @RequestMapping(value = "/list", method = {RequestMethod.GET})
    @ResponseBody
    public LayuiAdminServerResponse index(@RequestParam(value = "page", defaultValue = "1") int page, @RequestParam(value = "limit", defaultValue = "10") int limit, AdminOperationLog adminOperationLog) {
        PageHelper.startPage(page, limit);
        List<AdminOperationLog> adminOperationLogList = adminOperationLogService.getAllByCondition(adminOperationLog);

        long count = adminOperationLogService.count();
        return LayuiAdminServerResponse.responseWithSuccess(adminOperationLogList, String.valueOf(count));
    }


    @NeedRole("administrator")
    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    @ResponseBody
    public ServerResponse<AdminOperationLog> show(@PathVariable("id") int id) {
    AdminOperationLog adminOperationLog = adminOperationLogService.findOneById(id);
        return ServerResponse.responseWithSuccess(adminOperationLog);
    }


    @NeedRole("administrator")
    @RequestMapping(value = "/{id}", method = {RequestMethod.DELETE})
    @ResponseBody
    public ServerResponse destroy(@PathVariable(value = "id") int id) {
        int i = adminOperationLogService.deleteById(id);

        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("删除失败请重试");
        }
    }

    @NeedRole("administrator")
    @RequestMapping(value = "/batchDelete", method = {RequestMethod.DELETE})
    @ResponseBody
    public ServerResponse batchDelete(@RequestParam(value = "ids[]") int[] ids) {
        int i = adminOperationLogService.batchDelete(ids);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("删除失败请重试");
        }
    }
}