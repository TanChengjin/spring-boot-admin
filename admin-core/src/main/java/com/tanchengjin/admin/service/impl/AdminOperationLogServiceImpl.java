package com.tanchengjin.admin.service.impl;

import com.tanchengjin.admin.model.dao.AdminOperationLogMapper;
import com.tanchengjin.admin.model.pojo.AdminOperationLog;
import com.tanchengjin.admin.service.AdminOperationLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * create by  admin_operation_log ServiceImpl
 *
 * @author TanChengjin
 * @version v1.0.0
 * @since v1.0.0
 */
@Service("adminOperationLogService")
public class AdminOperationLogServiceImpl implements AdminOperationLogService {

    @Autowired
    private AdminOperationLogMapper adminOperationLogMapper;

    public AdminOperationLog findOneById(int id) {
        AdminOperationLog adminOperationLog = adminOperationLogMapper.selectByPrimaryKey(id);
        return adminOperationLog;
    }

    //逻辑删除
    public int deleteById(int id) {
        AdminOperationLog adminOperationLog1 = new AdminOperationLog();
        adminOperationLog1.setId(id);
        adminOperationLog1.setDeletedAt(new Date());
        int i = adminOperationLogMapper.updateByPrimaryKeySelective(adminOperationLog1);
//        int i = adminOperationLogMapper.deleteByPrimaryKey(id);
        return i;

    }

    public int create(AdminOperationLog adminOperationLog) {
        int i = adminOperationLogMapper.insertSelective(adminOperationLog);
        return i;
    }

    public int updateById(AdminOperationLog adminOperationLog, int id) {
        adminOperationLog.setId(id);
        int i = adminOperationLogMapper.updateByPrimaryKeySelective(adminOperationLog);
        return i;
    }

    public List<AdminOperationLog> getAll() {
        List<AdminOperationLog> adminOperationLogList = adminOperationLogMapper.getAll();
        return adminOperationLogList;
    }

    public List<AdminOperationLog> getAllByCondition(AdminOperationLog adminOperationLog) {
        List<AdminOperationLog> adminOperationLogList = adminOperationLogMapper.getAllByCondition(adminOperationLog);
        return adminOperationLogList;
    }

    public int batchDelete(int[] ids) {
        int i = 0;
        i = adminOperationLogMapper.logicalBatchDeleteByPrimaryKey(ids);
        return i;
    }

    public long count() {
        return adminOperationLogMapper.count();
    }
}