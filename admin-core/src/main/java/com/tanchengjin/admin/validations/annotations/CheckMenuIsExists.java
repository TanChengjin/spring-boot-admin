package com.tanchengjin.admin.validations.annotations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author TanChengjin
 * @version v1.0.0
 * @email 18865477815@163.com
 */
@Documented
@Target(value = {ElementType.FIELD})
@Retention(value = RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CheckMenuIsExistsValidator.class)
public @interface CheckMenuIsExists {
    /**
     * 校验出错时返回的消息
     *
     * @return String
     */
    String message() default "不存在的菜单";

    /**
     * 分组校验
     */
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
