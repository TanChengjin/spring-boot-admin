package com.tanchengjin.admin.model.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 菜单表(admin_menu)实体类
 *
 * @author TanChengjin
 * @since 2021-11-15 17:05:52
 */
public class AdminMenu implements Serializable {

    private static final long serialVersionUID = 719408388984644601L;

    private Integer id;
    /**
     * 菜单名称
     */
    private String name;
    /**
     * url路径
     */
    private String url;
    /**
     * 是否为目录
     */
    private Boolean isDirectory;
    /**
     * 描述
     */
    private String description;
    /**
     * 图标
     */
    private String icon;
    /**
     * 上级菜单
     */
    private Integer parentId;

    private int sort;

    //extension
    private int[] permissionList;

    /**
     * 层级
     */
    private int level;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    private List<AdminMenu> children;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getIsDirectory() {
        return isDirectory;
    }

    public void setIsDirectory(Boolean isDirectory) {
        this.isDirectory = isDirectory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public List<AdminMenu> getChildren() {
        return children;
    }

    public void setChildren(List<AdminMenu> children) {
        this.children = children;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }


    public int[] getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(int[] permissionList) {
        this.permissionList = permissionList;
    }

    @Override
    public String toString() {
        return "AdminMenu{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", isDirectory=" + isDirectory +
                ", description='" + description + '\'' +
                ", icon='" + icon + '\'' +
                ", parentId=" + parentId +
                ", sort=" + sort +
                ", permissionList=" + Arrays.toString(permissionList) +
                ", level=" + level +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", children=" + children +
                '}';
    }
}

