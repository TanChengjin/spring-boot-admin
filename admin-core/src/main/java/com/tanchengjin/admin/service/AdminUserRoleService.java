package com.tanchengjin.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tanchengjin.admin.model.pojo.AdminUserRole;

/**
 * @author TanChengjin
 * @version v1.0.0
 * @email 18865477815@163.com
 */
public interface AdminUserRoleService extends IService<AdminUserRole> {
}
