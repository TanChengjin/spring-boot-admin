package com.tanchengjin.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tanchengjin.admin.model.dao.AdminPermissionMapper;
import com.tanchengjin.admin.model.pojo.AdminPermission;
import com.tanchengjin.admin.model.pojo.AdminRole;

import java.util.List;

/**
 * @author TanChengjin
 * @since 2021-11-15 17:06:56
 */
public interface AdminPermissionService extends IService<AdminPermission> {
    List<AdminPermission> getAllByMenuId(int id);

    List<AdminPermission> getAll();

    /**
     * 获取菜单树
     */
    List<AdminPermission> getMenuTree();

    int addPermissionByRoleId(int roleId, List<Integer> permissionList);

    List<AdminPermission> getPermissionsByRoleId(int id);
    List<AdminPermission> getPermissionsByRoleIds(List<Integer> ids);
}
