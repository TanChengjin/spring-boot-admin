package com.tanchengjin.admin.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanchengjin.admin.enumerations.ConfigKey;
import com.tanchengjin.admin.model.dao.AdminConfigEmailMapper;
import com.tanchengjin.admin.model.dao.AdminConfigMapper;
import com.tanchengjin.admin.model.pojo.AdminConfig;
import com.tanchengjin.admin.model.pojo.AdminConfigEmail;
import com.tanchengjin.admin.model.vo.ConfigWebsiteVO;
import com.tanchengjin.admin.service.AdminConfigService;
import com.tanchengjin.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 设置控制器
 */
@Controller
@RequestMapping("${app.admin.prefix}/settings")
public class SettingManagerController {
    @Autowired
    private AdminConfigEmailMapper emailMapper;
    /**
     * Service
     */
    @Autowired
    private AdminConfigService adminConfigService;

    /**
     * Mapper
     */
    @Autowired
    private AdminConfigMapper adminConfigMapper;


    @RequestMapping(value = "/email", method = RequestMethod.GET)
    public String emailPage(Model view) {
        AdminConfigEmail adminConfigEmail = emailMapper.selectByPrimaryKey(1);
        view.addAttribute("config", adminConfigEmail);
        return "/admin/setting/email";
    }

    @RequestMapping(value = "/email", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse email(AdminConfigEmail adminConfigEmail) {
        adminConfigEmail.setId(1);
        int i = emailMapper.updateByPrimaryKeySelective(adminConfigEmail);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess("success");
        }
        return ServerResponse.responseWithFailureMessage("success");
    }

    /**
     * 站点配置 基础信息
     *
     * @return
     */
    @RequestMapping(value = "/website", method = RequestMethod.GET)
    public String websitePage(Model model) {
        AdminConfig adminConfig = adminConfigMapper.selectByKey(ConfigKey.WEBSITE.getKey());
        String content = adminConfig == null ? "" : adminConfig.getContent();
        ConfigWebsiteVO configWebsiteVO = new ConfigWebsiteVO();
        try {
            configWebsiteVO = new ObjectMapper().readValue(content, ConfigWebsiteVO.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        model.addAttribute(ConfigKey.WEBSITE.getKey(), configWebsiteVO);
        return "/admin/setting/website";
    }

    /**
     * 信息保存，如果不存在则新增
     *
     * @return
     */
    @RequestMapping(value = "/website", method = RequestMethod.PUT)
    @ResponseBody
    public ServerResponse website(@RequestBody ConfigWebsiteVO websiteVO) {

        int i = adminConfigService.updateByKey(websiteVO, ConfigKey.WEBSITE);
        if (i <= 0) {
            return ServerResponse.responseWithFailureMessage("error");
        }
        return ServerResponse.responseWithSuccess("success");
    }


    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    @ResponseBody
    public ServerResponse<AdminConfig> show(@PathVariable("id") int id) {
        AdminConfig adminConfig = adminConfigService.findOneById(id);
        return ServerResponse.responseWithSuccess(adminConfig);
    }

    @RequestMapping(value = "/{id}/edit", method = {RequestMethod.GET})
    public String edit(Model view, @PathVariable(value = "id") int id) {
        AdminConfig adminConfig = adminConfigService.findOneById(id);
        view.addAttribute("adminConfig", adminConfig);
        return "/adminConfig/edit";
    }

    /**
     * <p>
     * POST http://www.example.com HTTP/1.1
     * Content-Type: application/json;charset=UTF-8
     * </p>
     *
     * @return ServerResponse
     */
    @RequestMapping(value = "/{id}", method = {RequestMethod.PUT})
    @ResponseBody
    public ServerResponse update(@RequestBody AdminConfig adminConfig, @PathVariable("id") int id) {
        int i = adminConfigService.updateById(adminConfig, id);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("更新失败，请重试");
        }
    }

    /**
     * <p>
     * POST http://www.example.com HTTP/1.1
     * Content-Type: application/x-www-form-urlencoded;charset=UTF-8
     * </p>
     *
     * @return ServerResponse
     */
    @RequestMapping(value = "/{id}/change", method = {RequestMethod.PUT})
    @ResponseBody
    public ServerResponse change(AdminConfig adminConfig, @PathVariable("id") int id) {
        int i = adminConfigService.updateById(adminConfig, id);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("更新失败，请重试");
        }
    }
}
