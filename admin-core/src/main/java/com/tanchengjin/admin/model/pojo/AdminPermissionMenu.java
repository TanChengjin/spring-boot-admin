package com.tanchengjin.admin.model.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * admin_permission_menu
 *
 * @author TanChengjin
 */
public class AdminPermissionMenu implements Serializable {

    private static final long serialVersionUID = 5223262309832665324L;

    private Integer id;

    /**
     * 菜单ID
     */
    private Integer adminMenuId;

    /**
     * 权限ID
     */
    private Integer adminPermissionId;

    private Date createdAt;

    private Date updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAdminMenuId() {
        return adminMenuId;
    }

    public void setAdminMenuId(Integer adminMenuId) {
        this.adminMenuId = adminMenuId;
    }

    public Integer getAdminPermissionId() {
        return adminPermissionId;
    }

    public void setAdminPermissionId(Integer adminPermissionId) {
        this.adminPermissionId = adminPermissionId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}