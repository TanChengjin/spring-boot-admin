package com.tanchengjin.admin.validations;

import javax.validation.constraints.NotEmpty;

/**
 * 文章分类校验类
 */
public class CategoryValidate {

    @NotEmpty(message = "分类名不能为空")
    private String name;

    private Integer parentId;

    private Integer sort;

    private Boolean isDirectory;

    private Boolean enable;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Boolean getDirectory() {
        return isDirectory;
    }

    public void setDirectory(Boolean directory) {
        isDirectory = directory;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}