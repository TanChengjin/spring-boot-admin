package com.tanchengjin.admin.model.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TanChengjin
 * @version v1.0.0
 * @email 18865477815@163.com
 */
public class LayuiTreeVO {
    private String title;
    private int id;
    private String field = "";
    /**
     * 当前节点是否展开
     */
    private boolean spread = true;
    /**
     * 当前节点是否选中
     */
    private boolean checked = false;
    private List<LayuiTreeVO> children = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public boolean isSpread() {
        return spread;
    }

    public void setSpread(boolean spread) {
        this.spread = spread;
    }

    public List<LayuiTreeVO> getChildren() {
        return children;
    }

    public void setChildren(List<LayuiTreeVO> children) {
        this.children = children;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
