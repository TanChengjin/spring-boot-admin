package com.tanchengjin.admin.exception;

public class NeedPermissionException extends RuntimeException {
    public NeedPermissionException(String message) {
        super(message);
    }

    public NeedPermissionException() {
        super("权限不足");
    }

}
