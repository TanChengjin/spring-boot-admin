package com.tanchengjin.admin.controller;

import com.github.pagehelper.PageHelper;
import com.tanchengjin.admin.aop.needRole.NeedRole;
import com.tanchengjin.admin.model.dao.AdminPermissionMapper;
import com.tanchengjin.admin.model.dao.AdminRoleMapper;
import com.tanchengjin.admin.model.dao.AdminRolePermissionMapper;
import com.tanchengjin.admin.model.pojo.AdminPermission;
import com.tanchengjin.admin.model.pojo.AdminRole;
import com.tanchengjin.admin.model.pojo.AdminRolePermission;
import com.tanchengjin.admin.service.AdminPermissionService;
import com.tanchengjin.admin.service.AdminRoleService;
import com.tanchengjin.admin.service.AdministratorService;
import com.tanchengjin.util.LayuiAdminServerResponse;
import com.tanchengjin.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author tanchengjin
 */
@Controller
@RequestMapping("${app.admin.prefix}/administrators/role")
public class AdminRoleManagerController {

    @Autowired
    private AdminRoleMapper adminRoleMapper;

    @Autowired
    private AdminPermissionMapper adminPermissionMapper;

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private AdminRolePermissionMapper adminRolePermissionMapper;

    @Autowired
    private AdminPermissionService adminPermissionService;

    @Autowired
    private AdminRoleService adminRoleService;

    @GetMapping("/index")
    public String index() {
        return "/admin/administrators/role/list";
    }

    @NeedRole(value = "administrator")
    @RequestMapping(value = "/list", method = {RequestMethod.GET})
    @ResponseBody
    public LayuiAdminServerResponse index(@RequestParam(value = "page", defaultValue = "1") int page, @RequestParam(value = "limit", defaultValue = "10") int limit) {
        PageHelper.startPage(page, limit);
        List<AdminRole> adminRoleList = adminRoleMapper.getAll();
        int count = adminRoleMapper.count();
        return LayuiAdminServerResponse.responseWithSuccess(adminRoleList, String.valueOf(count));
    }

    @RequestMapping(value = "/create", method = {RequestMethod.GET})
    public String create(Model model) {
        List<AdminPermission> permissionList = adminPermissionMapper.getAll();
        model.addAttribute("permissionList", permissionList);
        return "/admin/administrators/role/add";
    }

    @RequestMapping(value = "", method = {RequestMethod.POST})
    @ResponseBody
    public ServerResponse store(AdminRole adminRole) {
        int i = administratorService.createAdminRole(adminRole);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("创建失败请重试");
        }
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    @ResponseBody
    public ServerResponse<AdminRole> show(@PathVariable int id) {
        AdminRole adminRole = adminRoleMapper.selectByPrimaryKey(id);
        return ServerResponse.responseWithSuccess(adminRole);
    }

    @RequestMapping(value = "/{id}/edit", method = {RequestMethod.GET})
    public String edit(Model view, @PathVariable(value = "id") int id) {
        AdminRole adminRole = adminRoleMapper.selectByPrimaryKey(id);
        view.addAttribute("adminRole", adminRole);
        List<AdminPermission> allPermissionList = adminPermissionMapper.getAll();
        //通过角色查询已拥有的所有权限
        List<AdminRolePermission> ownPermissionList = adminRolePermissionMapper.selectByRoleId(adminRole.getId());

        for (AdminPermission adminPermission : allPermissionList) {
            for (AdminRolePermission ownPermission : ownPermissionList) {
                if (adminPermission.getId() == ownPermission.getAdminPermissionId()) {
                    adminPermission.setChecked(true);
                    break;
                }
            }
        }
        view.addAttribute("permissionList", allPermissionList);


        return "/admin/administrators/role/edit";
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.PUT})
    @ResponseBody
    public ServerResponse update(AdminRole adminRole, @PathVariable("id") int id) {
        adminRole.setId(id);
//        int i = adminRoleService.update(adminRole);
        int i = adminRoleMapper.updateByPrimaryKeySelective(adminRole);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("更新失败，请重试");
        }
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.DELETE})
    @ResponseBody
    public ServerResponse destroy(@PathVariable(value = "id") int id) {
        int i = adminRoleMapper.deleteByPrimaryKey(id);
        if (i >= 1) {
            return ServerResponse.responseWithSuccess();
        } else {
            return ServerResponse.responseWithFailureMessage("删除失败请重试");
        }
    }

    /**
     * 分配权限
     *
     * @param id 角色
     */
    @RequestMapping(value = "/{id}/allocPermission", method = {RequestMethod.GET})
    public String allocPermission(@PathVariable(value = "id") int id, Model view) {
        view.addAttribute("role_id", id);
        return "/admin/administrators/permission/alloc";
    }

    /**
     * 获取某角色下所有权限
     *
     * @param id 角色id
     */
    @RequestMapping(value = "/{id}/permissions", method = {RequestMethod.GET})
    @ResponseBody
    public ServerResponse<Object> permissions(@PathVariable(value = "id") int id) {
        List<AdminPermission> permissionsByRoleId = adminPermissionService.getPermissionsByRoleId(id);
        return ServerResponse.responseWithSuccess(permissionsByRoleId);
    }
}
