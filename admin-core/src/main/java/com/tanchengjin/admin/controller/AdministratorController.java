package com.tanchengjin.admin.controller;

import com.tanchengjin.admin.model.dao.AdminUserMapper;
import com.tanchengjin.admin.model.pojo.AdminUser;
import com.tanchengjin.admin.model.vo.AdministratorVO;
import com.tanchengjin.admin.service.AdminPermissionService;
import com.tanchengjin.admin.service.AdministratorService;
import com.tanchengjin.admin.validations.AdminInfoValidate;
import com.tanchengjin.admin.validations.ChangePwdValidate;
import com.tanchengjin.util.ServerResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 管理员控制器
 *
 * @author tanchengjin
 */
@Controller
@RequestMapping("${app.admin.prefix}/administrator")
public class AdministratorController {

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AdminUserMapper adminUserMapper;
    @Autowired
    private AdminPermissionService adminPermissionService;


    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse info() {
        AdministratorVO admin = administratorService.getAdmin();
        return ServerResponse.responseWithSuccess(admin);
    }

    /**
     * 当前管理员基本信息界面
     *
     * @return java.lang.String
     * @author TanChengjin
     * @version v1.0.0
     * @since v1.0.0
     */
    @RequestMapping(value = "/baseInfo", method = RequestMethod.GET)
    public String baseInfoPage(Model view) {
        AdminUser currentUser = administratorService.getCurrentUser();
        view.addAttribute("admin", currentUser);
        return "/admin/administrator/info";
    }

    /**
     * 修改管理员基本信息
     */
    @RequestMapping(value = "/baseInfo", method = RequestMethod.PUT)
    @ResponseBody
    public ServerResponse baseInfo(AdminInfoValidate validate) {
        //TODO
        Integer id = administratorService.getCurrentUser().getId();
        AdminUser adminUser = new AdminUser();
        adminUser.setId(id);
        BeanUtils.copyProperties(validate, adminUser);
        int i = adminUserMapper.updateByPrimaryKeySelective(adminUser);
        if (i <= 0) {
            return ServerResponse.responseWithFailureMessage("更新失败，请重试!");
        }
        return ServerResponse.responseWithSuccess("更新成功");
    }

    /**
     * 当前管理员密码修改界面
     */
    @RequestMapping(value = "/password", method = RequestMethod.GET)
    public String changePwdPage() {
        return "/admin/administrator/password";
    }

    @RequestMapping(value = "/password", method = RequestMethod.PUT)
    @ResponseBody
    public ServerResponse changePwd(@Validated ChangePwdValidate passwordVO) {
        if (!passwordVO.getPassword().equals(passwordVO.getPasswordConfirm())) {
            return ServerResponse.responseWithFailureMessage("新密码与确认密码不一致");
        }
        //获取原密码
        Integer id = administratorService.getCurrentUser().getId();
        AdminUser adminUser = adminUserMapper.selectByPrimaryKey(id);
        String password1 = adminUser.getPassword();
        //校验原密码是否正确
        if (passwordEncoder.matches(passwordVO.getOldPassword(), password1)) {
            int i = administratorService.changePassword(administratorService.getCurrentUser().getId(), passwordEncoder.encode(passwordVO.getPassword()));
            if (i >= 1) {
                return ServerResponse.responseWithSuccess("操作成功");
            }
        } else {
            return ServerResponse.responseWithFailureMessage("当前密码密码错误");
        }

        return ServerResponse.responseWithSuccess("修改密码失败");
    }

    /**
     * 当前用户的权限树
     */
    @GetMapping("/menuTree")
    @ResponseBody
    public ServerResponse<?> menuTree() {
        return ServerResponse.responseWithSuccess(adminPermissionService.getMenuTree());
    }

    /**
     * 当前用户的所有权限
     */
    @GetMapping("/menus")
    @ResponseBody
    public ServerResponse<?> menus() {
        return ServerResponse.responseWithSuccess(administratorService.getPermissions());
    }

    /**
     * 当前用户的所有角色
     */
    @GetMapping("/roles")
    @ResponseBody
    public ServerResponse<?> roles() {
        return ServerResponse.responseWithSuccess(administratorService.getRoles());
    }

    /**
     * 当前用户的所有角色id
     */
    @GetMapping("/roleIds")
    @ResponseBody
    public ServerResponse<?> roleIds() {
        return ServerResponse.responseWithSuccess(administratorService.getRoleIds());
    }
}
