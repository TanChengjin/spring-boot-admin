package com.tanchengjin.admin.config.freemarker;

import com.tanchengjin.admin.config.AdminConfig;
import com.tanchengjin.admin.config.freemarker.customMethod.NeedPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;

@ControllerAdvice
public class FreemarkerAdvice {

    @Value("${app.admin.prefix:admin}")
    private String adminPrefix;

    @Autowired
    private AdminConfig adminConfig;

    @ModelAttribute(name = "adminPrefix")
    public String getAdminPrefix() {
        return this.adminPrefix;
    }

    @ModelAttribute(name = "adminConfig")
    public AdminConfig getAdminConfig() {
        return this.adminConfig;
    }

    @ModelAttribute(name = "hasPermission")
    public NeedPermission getNeedPermission() {
        return new NeedPermission();
    }
}
