package com.tanchengjin.admin.validations.annotations;

import com.tanchengjin.admin.model.dao.AdminRoleMapper;
import com.tanchengjin.admin.model.pojo.AdminRole;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author TanChengjin
 * @version v1.0.0
 * @email 18865477815@163.com
 */
public class CheckRoleIsExistsValidator implements ConstraintValidator<CheckRoleIsExists, Integer> {
    @Autowired
    private AdminRoleMapper adminRoleMapper;

    @Override
    public void initialize(CheckRoleIsExists constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        AdminRole adminRole = adminRoleMapper.selectByPrimaryKey(integer);
        return adminRole != null;
    }
}
