package com.tanchengjin.admin.model.dao;

import com.tanchengjin.admin.model.pojo.AdminMenu;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AdminMenuMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AdminMenu record);

    int insertSelective(AdminMenu record);

    AdminMenu selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AdminMenu record);

    /**
     * 允许插入控制的parent_id
     */
    int updateByPrimaryKeySelectiveAllowParentEqualsNull(AdminMenu record);

    int updateByPrimaryKey(AdminMenu record);

    List<AdminMenu> getAll();

    long count();

    @Deprecated
    List<AdminMenu> getAllByParentId(Integer id);

    @Deprecated
    List<AdminMenu> getAllByParentIdAndOrderByIdDesc(Integer id);

    List<AdminMenu> getAllByParentIdEqualsNull();

    @Deprecated
    List<AdminMenu> getAllByParentIdEqualsNullAndOrderByIdDesc();

    @Deprecated
    List<AdminMenu> getAllByParentIdEqualsNullAndOrderByIdAsc();

    List<AdminMenu> getAllByParentIdEqualsNullAndOrderBy(String k, String v);

    List<AdminMenu> getAllByParentIdAndOrderBy(int id, String k, String v);

    //获取某一节点的最大排序值
    int getMaxSortByParentId(Integer parent_id);

    int getMinSortByParentId(Integer parent_id);

    //更新某字段的parent_id为0
    int updateParentIdIsNullById(int id);

    List<AdminMenu> selectByPermissionId(int id, String k, String v);

    /**
     * @param id 管理员id
     * @param k  排序key
     * @param v  排序value
     * @return 菜单
     */
    List<AdminMenu> selectByAdminId(int id, String k, String v);
}