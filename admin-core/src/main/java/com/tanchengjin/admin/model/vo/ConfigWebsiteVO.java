package com.tanchengjin.admin.model.vo;

public class ConfigWebsiteVO {
    private String websiteName;

    public String getWebsiteName() {
        return websiteName;
    }

    public void setWebsiteName(String websiteName) {
        this.websiteName = websiteName;
    }
}
