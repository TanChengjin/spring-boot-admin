package com.tanchengjin.admin.validations;

import javax.validation.constraints.Max;

/**
 * 管理员基本信息
 */
public class AdminInfoValidate {
    /**
     * 用户昵称
     */
    private String name;

    private String email;

    private String phone;

    private Integer sex;

    private String avatarUrl;
    @Max(value = 255,message = "最大255字符")
    private String signature;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}