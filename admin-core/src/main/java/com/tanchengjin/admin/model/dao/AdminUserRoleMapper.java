package com.tanchengjin.admin.model.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanchengjin.admin.model.pojo.AdminUserRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AdminUserRoleMapper extends BaseMapper<AdminUserRole> {
    int deleteByPrimaryKey(Integer id);

    int insert(AdminUserRole record);

    int insertSelective(AdminUserRole record);

    AdminUserRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AdminUserRole record);

    int updateByPrimaryKey(AdminUserRole record);

    List<AdminUserRole> selectByAdminUserId(int id);
}