package com.tanchengjin.admin.validations;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

/**
 * 管理员修改密码
 */
public class ChangePwdValidate {
    //当前密码
    @NotEmpty(message = "密码不能为空")
    private String oldPassword;
    //新密码
    @NotEmpty(message = "新密码不能为空")
    @Length(min = 6,max = 16,message = "最大16个字符最小6个字符")
    private String password;
    //确认密码
    @NotEmpty(message = "确认密码不能为空")
    private String passwordConfirm;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
