
package com.tanchengjin.admin.service;

import com.tanchengjin.admin.model.pojo.ChinaArea;

import java.util.List;

/**
 * china_area Service
 *
 * @author TanChengjin
 * @since
 */
public interface ChinaAreaService{

    ChinaArea findOneById(int id);

    int deleteById(int id);

    int create(ChinaArea record);

    int updateById(ChinaArea record,int id);

    List<ChinaArea>getAll();

    List<ChinaArea>getAllByCondition(ChinaArea condition);

    int batchDelete(int[] ids);

    int count();

    int countByCondition(ChinaArea chinaArea);
}