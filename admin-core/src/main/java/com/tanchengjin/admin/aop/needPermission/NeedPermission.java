package com.tanchengjin.admin.aop.needPermission;

import com.tanchengjin.admin.aop.needRole.NeedRole;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author TanChengjin
 * @version v1.0.0
 * @email 18865477815@163.com
 */
@Documented
@Target({java.lang.annotation.ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NeedPermission {
    String[] value();

    NeedRole.ResponseType responseType() default NeedRole.ResponseType.REST;
}
