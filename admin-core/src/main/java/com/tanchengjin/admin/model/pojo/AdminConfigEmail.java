package com.tanchengjin.admin.model.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * 邮箱服务配置表
 * @author TanChengjin
 * @since v1.0.0
 * @version v1.0.0
 */
//@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class AdminConfigEmail implements Serializable {

    private static final long serialVersionUID = 1791082720232008134L;

    private Integer id;
    @NotEmpty
    private String host;
    @NotEmpty
    private String port;
    @NotEmpty
    private String username;
    @NotEmpty
    private String password;


    public Integer getId()
    {
        return this.id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getHost()
    {
        return this.host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getPort()
    {
        return this.port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public String getUsername()
    {
        return this.username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return this.password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

}