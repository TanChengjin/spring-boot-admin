package com.tanchengjin.admin.config.freemarker.customMethod;

import com.tanchengjin.admin.model.pojo.AdminPermission;
import com.tanchengjin.admin.service.AdministratorService;
import com.tanchengjin.admin.utils.SpringUtil;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

public class NeedPermission implements TemplateMethodModelEx {
    private final static Logger logger = LoggerFactory.getLogger(NeedPermission.class);

    @Override
    public Object exec(List list) throws TemplateModelException {
        AdministratorService bean = SpringUtil.getBean(AdministratorService.class);
        if (list.size() <= 0) return false;
        String o = list.get(0).toString();
        List<AdminPermission> identifierPermission = bean.getIdentifierPermission();
        for (AdminPermission e : identifierPermission) {
            if (!StringUtils.isBlank(e.getKey()) && e.getKey().equals(o)) {
                return true;
            }
        }
        return false;
    }
}
