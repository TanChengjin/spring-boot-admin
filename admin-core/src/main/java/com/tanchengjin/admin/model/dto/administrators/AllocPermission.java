package com.tanchengjin.admin.model.dto.administrators;

import com.tanchengjin.admin.validations.annotations.CheckMenuIsExists;
import com.tanchengjin.admin.validations.annotations.CheckRoleIsExists;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * @author TanChengjin
 * @version v1.0.0
 * @email 18865477815@163.com
 */
public class AllocPermission {
    /**
     * 要分配权限的角色
     */
    @NotNull(message = "角色不允许为空")
    @Min(value = 1, message = "非法角色")
    @CheckRoleIsExists
    private int roleId;
    /**
     * 所分配的菜单(通过菜单查找对应的权限)
     */
    @NotNull(message = "菜单不允许为空")
    @Size(min = 1, message = "菜单不允许为空")
    @CheckMenuIsExists
    private List<Integer> menuIds = new ArrayList<>();

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public List<Integer> getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(List<Integer> menuIds) {
        this.menuIds = menuIds;
    }
}
