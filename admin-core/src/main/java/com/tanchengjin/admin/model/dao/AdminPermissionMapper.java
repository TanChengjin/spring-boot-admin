package com.tanchengjin.admin.model.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanchengjin.admin.model.pojo.AdminPermission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AdminPermissionMapper extends BaseMapper<AdminPermission> {
    int deleteByPrimaryKey(Integer id);

    int insert(AdminPermission record);

    int insertSelective(AdminPermission record);

    AdminPermission selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AdminPermission record);

    int updateByPrimaryKey(AdminPermission record);

    int count();

    List<AdminPermission> getAll();

    /**
     * 获取所有顶级菜单
     */
    List<AdminPermission> selectAndParentIdIsNull();

    /**
     * 获取子权限
     *
     * @param parentId 父权限id
     */
    List<AdminPermission> selectByParentId(Integer parentId);
}