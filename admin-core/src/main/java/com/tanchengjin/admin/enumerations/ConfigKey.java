package com.tanchengjin.admin.enumerations;

/**
 * 配置项 key
 *
 * @author TanChengjin
 */
public enum ConfigKey {
    WEBSITE("website", "站点配置");
    private final String key;
    private final String description;

    ConfigKey(String key, String description) {
        this.key = key;
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }
}
