package com.tanchengjin.admin.model.dao;

import com.tanchengjin.admin.model.pojo.AdminUser;
import com.tanchengjin.admin.model.pojo.AdminUserExtension;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AdminUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AdminUser record);

    int insertSelective(AdminUser record);

    AdminUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AdminUser record);

    int updateByPrimaryKey(AdminUser record);

    List<AdminUser> getAll();

    int count();

    int batchDeleteByPrimaryKey(int[] ids);

    AdminUser selectByUsername(String username);

    List<AdminUserExtension> selectAdminDetails();
    @Deprecated
    AdminUserExtension selectAdminDetailsById(int id);
}