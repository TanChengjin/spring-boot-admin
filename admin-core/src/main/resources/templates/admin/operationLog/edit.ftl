<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <title>管理员操作日志-编辑</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/static/layuiadmin/layui/css/layui.css" media="all">
</head>
<body>

<div class="layui-form" lay-filter="layuiadmin-form-role" id="layuiadmin-form-role" style="padding: 20px 30px 0 0;">

    <input type="hidden" name="id" value="${adminOperationLog.id?c}">

    <#include "form.ftl">

    <div class="layui-form-item layui-hide">
        <button class="layui-btn" lay-submit lay-filter="LAY-admin-operation-log-submit" id="LAY-admin-operation-log-submit">提交</button>
    </div>
</div>

<script src="/static/layuiadmin/layui/layui.js"></script>
<script>
    layui.config({
            base: '/static/layuiadmin/' //静态资源所在路径
            }).extend({
            index: 'lib/index' //主入口模块
            }).use(['index', 'form', 'laydate' ], function () {
            var $ = layui.$
            , form = layui.form;
        var laydate = layui.laydate;

        //日期时间范围
        laydate.render({
            elem: '#CREATED_AT',
            //datetime,time,month,year,en
            type: 'datetime',
            //范围
            range: false,
            //公历节日
            calendar: true,
            /**
             *
             * @param value 当前选中日期时间
             * @param date object {"year": 2021,"month": 11,"date": 23,"hours": 0,"minutes": 0,"seconds": 0}
             }
             */
            done: function (value, date) {

            }
        });
        //日期时间范围
        laydate.render({
            elem: '#UPDATED_AT',
            //datetime,time,month,year,en
            type: 'datetime',
            //范围
            range: false,
            //公历节日
            calendar: true,
            /**
             *
             * @param value 当前选中日期时间
             * @param date object {"year": 2021,"month": 11,"date": 23,"hours": 0,"minutes": 0,"seconds": 0}
             }
             */
            done: function (value, date) {

            }
        });
        //日期时间范围
        laydate.render({
            elem: '#DELETED_AT',
            //datetime,time,month,year,en
            type: 'datetime',
            //范围
            range: false,
            //公历节日
            calendar: true,
            /**
             *
             * @param value 当前选中日期时间
             * @param date object {"year": 2021,"month": 11,"date": 23,"hours": 0,"minutes": 0,"seconds": 0}
             }
             */
            done: function (value, date) {

            }
        });
            })
</script>
</body>
</html>