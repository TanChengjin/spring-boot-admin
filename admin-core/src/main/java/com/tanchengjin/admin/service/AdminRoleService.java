package com.tanchengjin.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tanchengjin.admin.model.pojo.AdminRole;
import com.tanchengjin.admin.model.pojo.AdminUserRole;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TanChengjin
 * @version v1.0.0
 * @email 18865477815@163.com
 */
public interface AdminRoleService extends IService<AdminRole> {
    int update(AdminRole adminRole);

    List<Integer> getIdsBySysUserId(int userid);

    List<AdminUserRole> getRolesBySysUserId(int userid);
}
