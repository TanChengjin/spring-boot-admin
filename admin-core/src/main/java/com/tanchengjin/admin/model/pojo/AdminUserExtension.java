package com.tanchengjin.admin.model.pojo;

import java.io.Serializable;
import java.util.List;

public class AdminUserExtension extends AdminUser implements Serializable {
    private static final long serialVersionUID = -2661190635173321051L;

    private List<AdminRole> roles;
    private List<AdminPermission> permissions;
    private List<AdminMenu> menus;

    public List<AdminRole> getRoles() {
        return roles;
    }

    public void setRoles(List<AdminRole> roles) {
        this.roles = roles;
    }

    public List<AdminPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<AdminPermission> permissions) {
        this.permissions = permissions;
    }

    public List<AdminMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<AdminMenu> menus) {
        this.menus = menus;
    }
}
