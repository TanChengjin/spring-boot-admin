package com.tanchengjin.admin.aop.needRole;

import java.lang.annotation.*;

/**
 * @author TanChengjin
 * @version v1.0.0
 * @email 18865477815@163.com
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NeedRole {
    String[] value();

    ResponseType responseType() default ResponseType.REST;


    public enum ResponseType {
        /**
         * 返回rest格式数据
         */
        REST,
        /**
         * 返回HTML视图
         */
        HTML
    }
}
