
package com.tanchengjin.admin.service;

import com.tanchengjin.admin.enumerations.ConfigKey;
import com.tanchengjin.admin.model.pojo.AdminConfig;
import com.tanchengjin.admin.model.vo.ConfigWebsiteVO;

import java.util.List;

/**
 * 数据库配置表
 *
 * @author TanChengjin
 * @since
 */
public interface AdminConfigService{

    AdminConfig findOneById(int id);

    int deleteById(int id);

    int create(AdminConfig record, ConfigKey configKey);

    int updateById(AdminConfig record, int id);

    List<AdminConfig>getAll();

    List<AdminConfig>getAllByCondition(AdminConfig condition);

    int batchDelete(int[] ids);

    int count();

    int updateByKey(ConfigWebsiteVO websiteVO, ConfigKey key);
}