package com.tanchengjin.admin.validations.annotations;

import com.tanchengjin.admin.model.dao.AdminMenuMapper;
import com.tanchengjin.admin.model.dao.AdminPermissionMapper;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

/**
 * @author TanChengjin
 * @version v1.0.0
 * @email 18865477815@163.com
 */
public class CheckMenuIsExistsValidator implements ConstraintValidator<CheckMenuIsExists, List<Integer>> {
//    @Autowired
//    private AdminMenuMapper adminMenuMapper;
    @Autowired
    private AdminPermissionMapper adminPermissionMapper;

    @Override
    public void initialize(CheckMenuIsExists constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(List<Integer> ids, ConstraintValidatorContext constraintValidatorContext) {
        for (Integer integer : ids) {
            if (adminPermissionMapper.selectByPrimaryKey(integer) == null) {
//                constraintValidatorContext.buildConstraintViolationWithTemplate("不存在的menu-" + integer).addConstraintViolation();
                return false;
            }
        }
        return true;
    }
}
