package com.tanchengjin.admin.controller;

import com.tanchengjin.admin.config.AdminConfig;
import com.tanchengjin.util.ServerResponse;
import com.tanchengjin.util.UeditorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author tanchengjin
 */
@RequestMapping("${app.admin.prefix}/upload")
@Component
@RestController
public class FileUploadController {
    private final static Logger logger = LoggerFactory.getLogger(FileUploadController.class);

    @Autowired
    private AdminConfig adminConfig;

    private final boolean storagePrefix = true;

    /**
     * 文件上传
     *
     * @param file file
     */
    @RequestMapping("/image")
    public ServerResponse uploadImage(MultipartFile file) {
        try {
            String s = this.uploadHandler(file);
            return ServerResponse.responseWithSuccess(buildResult(s));
        } catch (IOException e) {
            e.printStackTrace();
            return ServerResponse.responseWithFailureMessage("文件保存失败，请重试");
        }
    }

    /**
     * layuieditor编辑器上传文件
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/layuieditor/image",method = {RequestMethod.POST})
    public Map uploadImageLayuieditor(MultipartFile file) {
        try {
            String s = this.uploadHandler(file);
            return layuiEditor(s, 0);
        } catch (IOException e) {
            e.printStackTrace();
            return layuiEditor("", 1);
        }
    }

    private Map layuiEditor(String src, int code) {
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        HashMap<String, Object> stringObjectHashMap1 = new HashMap<>();
        stringObjectHashMap1.put("src", src);
        stringObjectHashMap1.put("title", "title");
        stringObjectHashMap.put("data", stringObjectHashMap1);
        stringObjectHashMap.put("code", code);
        return stringObjectHashMap;
    }


    @RequestMapping("/ueditor")
    public UeditorResponse ueditorUpload(MultipartFile upfile) {
        String url = "";
        String type = upfile.getContentType();
        String size = String.valueOf(upfile.getSize());
        String original = upfile.getOriginalFilename();
        String title = "";
        logger.info(String.valueOf(upfile.getSize()));
        try {
            url = uploadHandler(upfile);
        } catch (IOException e) {
            e.printStackTrace();
            return UeditorResponse.responseWithFailure();
        }
        return UeditorResponse.responseWithSuccess(url, title, original, type, size);
    }

    /**
     * 存储文件并返回文件的存储路径
     *
     * @param file file
     * @return String
     * @throws IOException
     */
    public String uploadHandler(MultipartFile file) throws IOException {
        String dir = "/upload/images/";
        String datePath = this.generateDatetimePath();

        String filename = this.randomFilename(this.getFileSuffix(file));
        String filePath = dir + datePath + "/" + filename;

        String storagePath = getStoragePath();
        File target = new File(storagePath + filePath);

        File spt = new File(storagePath + dir + datePath + File.separator);
        if (!spt.exists()) {
            if (!spt.mkdirs()) {
                throw new IOException("file mkdir error");
            }
        }
        file.transferTo(target);
        if (this.storagePrefix) filePath = "/storage" + filePath;
        return filePath;
    }

    /**
     * 获取存储路径
     */
    private String getStoragePath() {
        String storagePath = adminConfig.getStoragePath();
        //去除尾部特殊符号
        if (storagePath.endsWith("/") || storagePath.endsWith("\\")) {
            storagePath = storagePath.substring(0, storagePath.length() - 1);
        }
        return storagePath;
    }

    /**
     * 生成日期格式的存储路径
     *
     * @return String
     */
    public String generateDatetimePath() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        return simpleDateFormat.format(new Date());
    }


    /**
     * 获取文件后缀
     *
     * @param file MultipartFile
     * @return String
     */
    public String getFileSuffix(MultipartFile file) {
        String originalFilename = file.getOriginalFilename();
        assert originalFilename != null : "file must not null";
        int i = originalFilename.lastIndexOf(".");
        String suffix = "";
        if (i >= 1) {
            suffix = originalFilename.substring(i);
        }
        return suffix;
    }

    /**
     * 生成随机文件名
     *
     * @param suffix String
     * @return String
     */
    private String randomFilename(String suffix) {
        return UUID.randomUUID().toString() + suffix;
    }

    private String randomFilename() {
        return UUID.randomUUID().toString();
    }

    private Map buildResult(String src) {
        HashMap<String, String> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("src", src);
        return objectObjectHashMap;
    }
}
